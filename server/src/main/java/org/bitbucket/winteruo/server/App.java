package org.bitbucket.winteruo.server;

import java.io.File;

import org.bitbucket.winteruo.api.data.ServerConfiguration;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.listeners.ICoreListener;
import org.bitbucket.winteruo.server.logger.WinterLogger;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	ICoreListener listener = new ICoreListener() {
			
			public void onStopNetworkServer() {
				// TODO Auto-generated method stub
				
			}
			
			public void onStartNetworkServer() {
				// TODO Auto-generated method stub
				
			}
			
			public void onStart() {
				
			}
			
			public void onServerConfiguration(ServerConfiguration configuration) {
				// TODO Auto-generated method stub
				
			}
			
			public void onInit() {
				// TODO Auto-generated method stub
				
			}
		};
    	
	   String home = System.getProperty("user.home") + File.separator + ".winteruo";
	   
       ICore core =  new Core(home);
       
       core.addCoreListener(listener);
       core.startServer();
        
        
        
    }
}
