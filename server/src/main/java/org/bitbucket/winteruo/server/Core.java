package org.bitbucket.winteruo.server;

import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

import org.apache.commons.io.FileUtils;
import org.bitbucket.winteruo.api.data.CityInfo;
import org.bitbucket.winteruo.api.data.MapsInfo;
import org.bitbucket.winteruo.api.data.ServerConfiguration;
import org.bitbucket.winteruo.api.interfaces.IAuthenticationManager;
import org.bitbucket.winteruo.api.interfaces.IConfigurationsManager;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.IFileManager;
import org.bitbucket.winteruo.api.interfaces.IWinterComponentsManager;
import org.bitbucket.winteruo.api.listeners.ICoreListener;
import org.bitbucket.winteruo.api.logger.IWinterLogger;
import org.bitbucket.winteruo.api.model.MapFileInfo;
import org.bitbucket.winteruo.api.model.Point3D;
import org.bitbucket.winteruo.api.model.ServerInfo;
import org.bitbucket.winteruo.api.utils.InvokeUtils;
import org.bitbucket.winteruo.server.components.AuthenticationManager;
import org.bitbucket.winteruo.server.components.ConfigurationsManager;
import org.bitbucket.winteruo.server.components.FileManager;
import org.bitbucket.winteruo.server.components.SerializerManager;
import org.bitbucket.winteruo.server.components.WinterComponentsManager;
import org.bitbucket.winteruo.server.components.World;
import org.bitbucket.winteruo.server.logger.WinterLogger;
import org.bitbucket.winteruo.server.network.NetworkServer;
import org.bitbucket.winteruo.server.utils.AppInfo;


/***
 * First class called when application start
 * @author tgiachi
 *
 */
public class Core implements ICore {

	private static WinterLogger logger = WinterLogger.getLogger(Core.class);

	@Getter
	private ServerConfiguration serverConfiguration;

	@Getter
	private IConfigurationsManager configurationsManager;

	@Getter
	private IWinterComponentsManager winterComponentsManager;

	@Getter
	private IAuthenticationManager authenticationManager;
	
	@Getter
	private IFileManager fileManager;

	@Getter
	private MapsInfo mapsInfo;
	@Getter
	private ServerInfo serverInfo;

	@Getter
	private NetworkServer networkServer;

	private List<ICoreListener> coreListeners = new ArrayList<ICoreListener>();

	/***
	 * create new Instance of Core
	 * @param serverConfiguration
	 */
	public Core(String rootDirectory) {
		this.configurationsManager = new ConfigurationsManager(rootDirectory, logger);

		this.networkServer = new NetworkServer(this);

		initConfiguration();
	
		initFileManager();
		
		loadMaps();

		initServerInfo();

		initAutheticationManager();

		init();

		printInfo();
		
		initWorld();



	}

	private void initWorld()
	{
		World.getInstance().setCore(this);
		World.getInstance().init();
	}
	
	private void initConfiguration()
	{
		this.serverConfiguration = (ServerConfiguration) configurationsManager.loadConfiguration(ServerConfiguration.class);

		if (serverConfiguration == null)
		{
			logger.warn("Configuration is empty!! Create default configuration!! PLEASE MODIFY CONFIGURATION UNDER %s", configurationsManager.getRootDirectory());

			configurationsManager.saveConfiguration(ServerConfiguration.class, new ServerConfiguration(AppInfo.APP_NAME, 7775, "0.0.0.0", "/opt/uodata/"));

			System.exit(0);
		}

		InvokeUtils.invokeListeners(coreListeners, "onServerConfiguration", new Class<?>[] {ServerConfiguration.class} , this.serverConfiguration);
	}

	private void initFileManager()
	{
		fileManager = new FileManager(this);
		
	}
	
	private void loadMaps()
	{
		try
		{
//			//FOR TEST
			MapsInfo test = new MapsInfo();
			
			List<CityInfo> cities = new ArrayList<CityInfo>();
			
			MapFileInfo info = new MapFileInfo(1, 4096, "Trammel", cities);
			
			cities.add( new CityInfo("New Heaven bank", "New Heaven", 1150168, new Point3D(3667, 2625, 0), 1));
			cities.add( new CityInfo("The Bountiful Harvest Inn", "New Heaven", 0, new Point3D(3503, 2574, 0), 1));
			
			cities.add( new CityInfo("The Empath Abbey", "Yew", 1075072, new Point3D(633, 858, 0), 1));
			cities.add( new CityInfo("The Barnacle", "Minoc", 1075073, new Point3D(2476, 413, 15), 1));
			cities.add( new CityInfo("The Wayfarer's Inn", "Britan", 1075074, new Point3D(1602, 1591, 20), 1));
			cities.add( new CityInfo("The Traveler's Inn", "Trinsic", 1075076, new Point3D(1845, 2745, 0), 1));
			cities.add( new CityInfo("The Mercenary Inn", "Jhelom", 1075078, new Point3D(1374, 3826, 0), 1));
			cities.add( new CityInfo("The Falconer's Inn", "Skara Brae", 1075079, new Point3D(618, 2234, 0), 1));
			cities.add( new CityInfo("The Ironwood Inn", "Vesper", 1075078, new Point3D(2771, 976, 0), 1));
			
			test.setCitiesInfo(cities);
			
			test.addMap(info);
			
			info = new MapFileInfo(0, 4096, "Felucca", null);
			test.addMap(info);

			info = new MapFileInfo(2, 4096, "Ilshenar", null);
			test.addMap(info);

			this.getConfigurationsManager().saveConfiguration(MapsInfo.class, test);
			
				
			this.mapsInfo = (MapsInfo) this.getConfigurationsManager().loadConfiguration(MapsInfo.class);
			
			
			
			if (mapsInfo != null)
			{
				getFileManager().prepareMap(mapsInfo.getMaps().toArray(new MapFileInfo[mapsInfo.getMaps().size()]));
				
			}
			else
			{
				logger.error("config\\MapsInfo.xml not found! WinterUO cannot start!");
				System.exit(1);
			}
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during load maps files! %s", ex.getMessage());
			System.exit(1);
		}
	}
	
	private void initAutheticationManager()
	{
		this.authenticationManager = new AuthenticationManager(this);
	}

	private void initServerInfo()
	{
		try
		{
			this.serverInfo = new ServerInfo();
			this.serverInfo.setAddress(Inet4Address.getLocalHost());
			this.serverInfo.setPort(serverConfiguration.getServerPort());
			this.serverInfo.setServerName(serverConfiguration.getServerName());
		}
		catch(Exception ex)
		{

		}
	}

	/**
	 * Print server header
	 */
	private void printInfo()
	{
		logger.info(AppInfo.getApplication());
		logger.info("Web: %s", AppInfo.APP_WEBSITE);
		logger.info("Java version: %s", System.getProperty("java.version"));
		logger.info("Processors: %s CPU Architecture: %s Available Heap RAM: %s", Runtime.getRuntime().availableProcessors(), System.getProperty ("os.arch"), FileUtils.byteCountToDisplaySize(Runtime.getRuntime().maxMemory()));

		logger.info("Server configuration: Listening %s port: %s ", getServerConfiguration().getServerAddress(), getServerConfiguration().getServerPort());
		logger.info("Server configuration: UO Data directory: %s", getServerConfiguration().getUoDirectory());
	}

	/**
	 * Init components
	 */
	private void init()
	{
		addShutdownHook();
		SerializerManager.getInstance();
	}

	private void addShutdownHook()
	{
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			public void run() {
				try {
					logger.info("Shutdown %s...", AppInfo.APP_NAME);
					networkServer.stop();
					winterComponentsManager.shutdown();
					logger.info("All services stopped...exiting", AppInfo.APP_NAME);

				} catch (Exception e) {

					e.printStackTrace();
				}				
			}
		}));
	}

	public void startNetworkServer() throws Exception
	{		
		networkServer.start();

		InvokeUtils.invokeListeners(coreListeners, "onStartNetworkServer");

	}

	public void stopNetworkServer() throws Exception
	{
		if (networkServer != null)
		{
			networkServer.start();
		}
		else
			throw new Exception("Network server not started!");

		InvokeUtils.invokeListeners(coreListeners, "onStopNetworkServer");

	}


	/***
	 * Start WinterUO network server
	 * @return
	 * @throws Exception
	 */
	public boolean startServer() throws Exception
	{
		try
		{
			InvokeUtils.invokeListeners(coreListeners, "onStart");

			this.winterComponentsManager = new WinterComponentsManager(this);

			startNetworkServer();

		}
		catch(Exception ex)
		{
			throw new Exception(ex);
		}

		return true;
	}

	public boolean stopServer() throws Exception
	{
		try
		{
			InvokeUtils.invokeListeners(coreListeners, "onStop");

		}
		catch(Exception ex)
		{
			throw new Exception(ex);
		}

		return true;

	}

	public boolean addCoreListener(ICoreListener listener) {
		return coreListeners.add(listener);
	}

	public IWinterLogger getLogger(Class<?> classz) {
		return WinterLogger.getLogger(classz);

	}
}
