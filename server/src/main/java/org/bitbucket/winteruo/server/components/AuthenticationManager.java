package org.bitbucket.winteruo.server.components;

import java.util.ArrayList;
import java.util.Date;

import org.bitbucket.winteruo.api.interfaces.IAuthenticationManager;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.logger.IWinterLogger;
import org.bitbucket.winteruo.api.model.Account;
import org.bitbucket.winteruo.api.model.AccountCharacter;
import org.bitbucket.winteruo.api.model.AccountLevel;
import org.bitbucket.winteruo.api.model.AccountRepository;


public class AuthenticationManager implements IAuthenticationManager {
	
	private ICore core;

	private AccountRepository repository;
	
	private IWinterLogger logger;
			
	public AuthenticationManager(ICore core) {
	
		this.core = core;
		this.logger = core.getLogger(getClass());
		
		checkDefaultRepository();
	}
	
	
	public Account authenticate(String username, String password)
	{	
		for (Account account: repository.getAccounts())
		{
			if (account.getUsername().equals(password))
			{
				if (account.getPassword().equals(password))
				{
					account.setLastLoginDate(new Date());
					saveRepository();
					return account;
				}
			}
		}
		
		return null;
	}
	
	
	
	private void checkDefaultRepository()
	{
		if (core.getConfigurationsManager().loadConfiguration(AccountRepository.class) == null)
		{
			logger.warn("Inizialing default Accounts repository...");
			repository = new AccountRepository();
			Account account = new Account(repository.nextId(), "admin", "admin", new Date(), null, false, AccountLevel.ADMINISTRATOR, null);
		//	account.setCharacters( new ArrayList<AccountCharacter>());
		//	account.getCharacters().add(new AccountCharacter(repository.nextId(), "Administrator", ""));
			
			
			repository.addAccount(account);

			core.getConfigurationsManager().saveConfiguration(AccountRepository.class, repository);
		}
		else
		{
			repository = (AccountRepository) core.getConfigurationsManager().loadConfiguration(AccountRepository.class);
		}
		
		
	}
	
	private void saveAccountRepository()
	{
		core.getConfigurationsManager().saveConfiguration(AccountRepository.class, repository);
	}
	
	private void saveRepository()
	{
		core.getConfigurationsManager().saveConfiguration(AccountRepository.class, repository);
	}

	
	public boolean addCharacter(Account clientAccount, AccountCharacter newCharacter)
	{
		for (Account account: repository.getAccounts())
		{
			if (account.equals(clientAccount))
			{
				
				account.getCharacters().add(newCharacter);
				saveAccountRepository();
				return true;
			}
			
		}
		
		return false;
	}
	
	public AccountCharacter getCharacterByName(Account account, String characterName)
	{
		for (Account acc: repository.getAccounts())
		{
			for (AccountCharacter character: acc.getCharacters())
			{
				if (character.getName().equals(characterName))
					return character;
			}
		}
		
		return null;
	}
	
	public Account reloadAccount(int serialId)
	{
		for (Account account: repository.getAccounts())
		{
			if (account.getSerialId() == serialId)
				return account;
		}
		
		return null;
	}
	

}
