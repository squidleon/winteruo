package org.bitbucket.winteruo.server.components;

import java.io.File;

import lombok.Getter;

import org.apache.commons.io.FileUtils;
import org.bitbucket.winteruo.api.interfaces.IConfigurationsManager;
import org.bitbucket.winteruo.api.logger.IWinterLogger;


/**
 * Component between XML and Objects
 * @author tgiachi
 *
 */
public class ConfigurationsManager implements IConfigurationsManager {


	private static IWinterLogger logger;

	private SerializerManager serializerManager;
	@Getter
	private String rootDirectory;
	private String configDirectory;


	public ConfigurationsManager(String rootDirectory, IWinterLogger logger) {
		ConfigurationsManager.logger = logger.newInstance(getClass());
		this.rootDirectory = rootDirectory;

		initSerializerManager();
		initConfigurationsDirectory();
	}

	private void initSerializerManager()
	{
		this.serializerManager = SerializerManager.getInstance();
	}

	private void initConfigurationsDirectory()
	{
		try
		{
			if (!new File(rootDirectory + File.separator + "config" + File.separator).exists() )
			{
				new File(rootDirectory + File.separator+ "config" + File.separator).mkdirs();
			}

			configDirectory = rootDirectory + File.separator + "config" + File.separator;

		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during load configuration directory: %s => %s", configDirectory, ex.getMessage());
		}
	}
	/* (non-Javadoc)
	 * @see org.bitbucket.winteruo.server.components.IConfigurationsManager#saveConfiguration(java.lang.Class, java.lang.Object)
	 */
	public boolean saveConfiguration(Class<?> classz, Object data)
	{
		try
		{
			String xml = serializerManager.toXML(data);
			logger.debug("to XML class: %s",data.getClass().getSimpleName());

			FileUtils.writeStringToFile(new File(configDirectory + classz.getSimpleName() + ".xml"), xml);
			
			return true;
		}
		catch(Exception ex){
			logger.exception(ex, "Error during save configuration: %s" ,ex.getMessage());
		}

		return true;
	}
	/* (non-Javadoc)
	 * @see org.bitbucket.winteruo.server.components.IConfigurationsManager#loadConfiguration(java.lang.Class)
	 */
	public <E> Object loadConfiguration(Class<?> classz)
	{
		if (new File(configDirectory + classz.getSimpleName() + ".xml").exists())
		{
			try
			{
				String xml = FileUtils.readFileToString(new File(configDirectory + classz.getSimpleName() + ".xml"));		
				return serializerManager.fromXML(xml);
			}
			catch(Exception ex)
			{
				logger.exception(ex, "Errore during load configuration: %s ==> %s", classz.getSimpleName(), ex.getMessage());
			}
		}

		return null;

	}
	
	public boolean saveBinaryConfiguration(Class<?> classz, Object object)
	{
		try
		{
			return SerializerManager.getInstance().binarySerialize(configDirectory + classz.getSimpleName() + ".bin", object);
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during saving binary configuration: %s" ,ex.getMessage());
		}
		
		return false;
	}
	
	public <E> Object loadBinaryConfiguration(Class<?> classz)
	{
		if (new File(configDirectory + classz.getSimpleName() + ".bin").exists())
		{
			try
			{
					
				return serializerManager.binaryDeserialization(configDirectory + classz.getSimpleName() + ".bin", classz);
			}
			catch(Exception ex)
			{
				logger.exception(ex, "Errore during load configuration: %s ==> %s", classz.getSimpleName(), ex.getMessage());
			}
		}

		return null;

	}


}
