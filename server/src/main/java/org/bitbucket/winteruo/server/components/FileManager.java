package org.bitbucket.winteruo.server.components;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bitbucket.winteruo.api.files.IdxFileReader;
import org.bitbucket.winteruo.api.files.MapFileReader;
import org.bitbucket.winteruo.api.files.SkillsFileEntry;
import org.bitbucket.winteruo.api.files.SkillsMulFileReader;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.IFileManager;
import org.bitbucket.winteruo.api.logger.IWinterLogger;
import org.bitbucket.winteruo.api.model.MapFileInfo;
import org.bitbucket.winteruo.api.model.MapLocation;
import org.bitbucket.winteruo.api.model.MapTile;
import org.bitbucket.winteruo.api.model.SkillGame;
import org.bitbucket.winteruo.api.model.SkillInfo;
import org.bitbucket.winteruo.api.model.SkillLockFlag;
import org.bitbucket.winteruo.api.model.SkillsCache;
import org.bitbucket.winteruo.server.files.FileReadersFactoryImpl;

public class FileManager implements IFileManager {

	private ICore core;
	private IWinterLogger logger;

	private FileReadersFactoryImpl frFactory = new FileReadersFactoryImpl();

	private IdxFileReader skillsIDXFileReader;
	private SkillsMulFileReader skillsFileReader;


	private HashMap<MapFileInfo, MapFileReader> maps = new HashMap<MapFileInfo, MapFileReader>();

	private SkillsCache skillsCache;


	public FileManager(ICore core) {
		this.core = core;
		this.logger = core.getLogger(getClass());
		init();
	}


	private void init()
	{
		try
		{
			readSkills();
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during reader IDX and MUL files: %s", ex.getMessage());

			System.exit(1);
		}
	}

	private void readSkills() throws Exception
	{
		skillsCache = (SkillsCache) core.getConfigurationsManager().loadBinaryConfiguration(SkillsCache.class);

		if (skillsCache == null)
		{
			skillsCache = new SkillsCache();
			skillsIDXFileReader = frFactory.createSkillsIdxFileReader(new File(core.getServerConfiguration().getUoDirectory() + File.separator + "skills.idx"));
			skillsFileReader  =  frFactory.createSkillsMulFileReader(new File(core.getServerConfiguration().getUoDirectory() + File.separator + "skills.mul"),skillsIDXFileReader);

			logger.info("Skills found: %s", skillsFileReader.getAllEntries().size());
			logger.info("Builing skills cache...");
			
			for (SkillInfo entry: getSkillfromFile())
			{
				
				skillsCache.addSkillGame(new SkillGame(entry, 1, 1, 100, SkillLockFlag.Up));
			}
			
			core.getConfigurationsManager().saveBinaryConfiguration(SkillsCache.class, skillsCache);
			logger.info("Builing skills cache...done", skillsFileReader.getAllEntries().size());
		}
	}


	public SkillInfo getSkillInfoById(int id)
	{
		SkillInfo entry = skillsCache.getSkills().get(id).getBaseSkill();

		if (entry != null)
		{
			return new SkillInfo(id, entry.getName());
		}

		return null;
	}

	public List<SkillInfo> getSkillsInfo()
	{
		
		List<SkillInfo> skillsInfo = new ArrayList<SkillInfo>();

		int i=0;
		for (SkillGame entry: skillsCache.getSkills())
		{
			skillsInfo.add(new SkillInfo(i, entry.getBaseSkill().getName()));	
			i++;
		}

		return skillsInfo;
	}

	
	public List<SkillInfo> getSkillfromFile()
	{
		List<SkillInfo> skillsInfo = new ArrayList<SkillInfo>();

		int i=0;
		for (SkillsFileEntry entry: skillsFileReader.getAllEntries())
		{
			
			skillsInfo.add(new SkillInfo(i, entry.getSkillName()));	
			i++;
		}

		return skillsInfo;
	}
	/* (non-Javadoc)
	 * @see org.bitbucket.winteruo.server.components.IFileManager#prepareMap(org.bitbucket.winteruo.api.model.MapFileInfo)
	 */
	public void prepareMap(MapFileInfo map)
	{
		try
		{
			String mapFullFile = String.format("%smap%s.mul", core.getServerConfiguration().getUoDirectory(), map.getMapId()); 
			logger.debug("Preloading map file: %s", mapFullFile);
			logger.info("Loading \"%s\" region...", map.getMapName());

			MapFileReader mp = frFactory.createMapFileReader(new File(mapFullFile), map.getSize());

			maps.put(map, mp);

		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during load map: %s", ex.getMessage());
		}

	}

	/* (non-Javadoc)
	 * @see org.bitbucket.winteruo.server.components.IFileManager#prepareMap(org.bitbucket.winteruo.api.model.MapFileInfo)
	 */
	public void prepareMap(MapFileInfo ... maps)
	{
		for (int i=0;i<maps.length;i++)
		{
			prepareMap(maps[i]);
		}
	}

	/* (non-Javadoc)
	 * @see org.bitbucket.winteruo.server.components.IFileManager#getMapTileByIndex(int, org.bitbucket.winteruo.api.model.MapLocation)
	 */
	public MapTile getMapTileByIndex(int idx, MapLocation location) throws Exception
	{
		for (MapFileInfo finfo: maps.keySet())
		{
			if (finfo.getMapId() == idx)
				return maps.get(finfo).getEntryAt(location);
		}

		throw new Exception("Invalid map index " + idx);
	}

	/* (non-Javadoc)
	 * @see org.bitbucket.winteruo.server.components.IFileManager#getMapTileByIndex(int, int, int)
	 */
	public MapTile getMapTileByIndex(int idx, int x, int y)  throws Exception
	{
		return getMapTileByIndex(idx, new MapLocation(x, y));
	}



	public MapFileInfo getMapFileInfoById(int id)
	{
		int idx = 0;
		for (MapFileInfo mInfo : maps.keySet())
		{
			if (idx == id)
				return mInfo;
			else
				idx++;

		}

		return null;
	}




}
