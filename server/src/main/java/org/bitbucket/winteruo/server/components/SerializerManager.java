package org.bitbucket.winteruo.server.components;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.bitbucket.winteruo.api.annotations.XMLAlias;
import org.bitbucket.winteruo.api.annotations.utils.ScanAnnotations;
import org.bitbucket.winteruo.server.logger.WinterLogger;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;


/**
 * Class for serialize information to files
 * @author tgiachi
 *
 */
public class SerializerManager {

	private static SerializerManager _instance = null;
	/**
	 * Get instance singleton
	 * @return
	 */
	public static SerializerManager getInstance() { if (_instance == null) _instance = new SerializerManager(); return _instance;}
	
	private static WinterLogger logger = WinterLogger.getLogger(SerializerManager.class);
	private XStream engine;
	private Kryo binSerializer;


	public SerializerManager() {

		logger.info("Initializing SerializerManager...");
		initXMLSerializer();
		initBinarySerializer();
	
	}

	private void initXMLSerializer()
	{
		engine = new XStream(new Dom4JDriver());
		engine.setMode(XStream.ID_REFERENCES);

		int numAliases = scanForXMLAlias();
		
		logger.info("SerializerManager initializated ==> found %s aliases", numAliases);
	}
	
	private void initBinarySerializer()
	{
		binSerializer = new Kryo();
	}
	
	/**
	 * Scan for XMLAlias annotations
	 * @return number of aliases found
	 */
	private int scanForXMLAlias()
	{
		
		int numAliasesFound = 0;
		try
		{
			Set<Class<?>> classez =  ScanAnnotations.getAnnotationFor(XMLAlias.class, "org.bitbucket.winteruo.api.data", "org.bitbucket.winteruo.api.model");

			for (Class<?> classz : classez)
			{
				String aliasName = "";
				XMLAlias annotation = classz.getAnnotation(XMLAlias.class);

				if (annotation.alias().isEmpty())
					aliasName = classz.getSimpleName();
				else
					aliasName = annotation.alias();

				engine.alias(aliasName, classz);
				numAliasesFound++;

				logger.debug("Found @XMLAlias on class: %s ==> Adding alias %s", classz.getName(), aliasName);

			}
			
			

		}
		catch(Exception ex)
		{
			logger.exception(ex, "Errore during scan @XMLAlias annotation: %s", ex.getMessage());
		}
		
		return numAliasesFound;
	}

	/**
	 * Convert Serializable object to XML
	 * @param obj
	 * @return
	 */
	public String toXML(Object obj)
	{
		return engine.toXML(obj);
	}
	/**
	 * Convert XML to Serializable
	 * @param xml
	 * @return
	 */
	public Object fromXML(String xml)
	{
		return engine.fromXML(xml);
	}
	/**
	 * Read form filename XML and try to convert to object
	 * @param filename
	 * @return
	 */
	public Object readXMLfromFile(String filename)
	{
		try
		{
			String xml = FileUtils.readFileToString(new File(filename));
			return fromXML(xml);
		}
		catch(Exception ex)
		{
			logger.error("Error during read file: %s ==> %s", filename, ex.getMessage());
		}
		return null;
	}
	/**
	 * Write XML to file
	 * @param filename
	 * @param Serializable object
	 * @return
	 */
	public boolean writeXMLtoFile(String filename, Object obj)
	{
		try
		{
			String xml = toXML(obj);
			FileUtils.writeStringToFile(new File(filename), xml, false);
			return true;
		}
		catch(Exception ex)
		{
			logger.error("Errore during write object: %s to filename: %s ==> %s",obj.getClass().getName(), filename, ex.getMessage());
		}

		return false;
	}
	
	/***
	 * Write binary object to file
	 * @param filename
	 * @param obj
	 * @return
	 */
	public boolean binarySerialize(String filename, Object obj)
	{
		try
		{
			Output out = new Output(new FileOutputStream(filename));
			binSerializer.writeObject(out, obj);
			out.flush();			
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Errore during binary serialization on file %s class %s", filename, obj.getClass().getSimpleName());
		}
		return false;
	}
	
	public <T> T binaryDeserialization(String filename, Class<T> classz)
	{
		try
		{
			Input in = new Input(new FileInputStream(filename));
			T ret = binSerializer.readObject(in, classz);
			in.close();
			
			return ret;
			
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during binary deserialize filename %s ==> %s", filename, ex.getMessage());
		}
		
		return null;
	}
}
