package org.bitbucket.winteruo.server.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bitbucket.winteruo.api.annotations.WinterComponent;
import org.bitbucket.winteruo.api.annotations.utils.ScanAnnotations;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.IWinterComponent;
import org.bitbucket.winteruo.api.interfaces.IWinterComponentsManager;
import org.bitbucket.winteruo.server.logger.WinterLogger;
import org.bitbucket.winteruo.server.network.NetworkServer;


/**
 * Manage user's components
 * @author tgiachi
 *
 */
public class WinterComponentsManager implements IWinterComponentsManager {
	private static WinterLogger logger = WinterLogger.getLogger(WinterComponentsManager.class);


	private List<IWinterComponent> components = new ArrayList<IWinterComponent>();


	private ICore core;

	public WinterComponentsManager(ICore core) {
		this.core = core; 
		logger.info("Initializing component manager...");

		scanComponents();
	}



	private void scanComponents() 
	{
		try
		{
			Set<Class<?>> classes =  ScanAnnotations.getAnnotationFor(WinterComponent.class, "org.bitbucket.winteruo.server.components");

			logger.info("Found %s @WinterComponent", classes.size());

			for (Class<?> classz : classes)
			{
				if (isObjectImplementsInterface(classz, IWinterComponent.class))					
				{
					IWinterComponent cmp = (IWinterComponent)classz.newInstance();
					addComponent(cmp);
				}

			}

		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during scan annotation @WinterComponent: %s", ex.getMessage());
		}
	}

	private boolean isObjectImplementsInterface(Class<?> src, Class<?> interfaceClass)
	{
		for (Class<?> classz : src.getInterfaces())
		{
			if (classz.equals(interfaceClass))
				return true;
		}
		return false;
	}

	public void addComponent(IWinterComponent component) throws Exception
	{
		WinterComponent annotation = component.getClass().getAnnotation(WinterComponent.class);
		logger.info("Adding component => %s %s -- %s", annotation.name(), annotation.version(), annotation.description());
		component.setLogger(logger);
		component.setCore(core);

		component.init();
		component.start();
		components.add(component);
	}

	public void shutdown() {
		for (IWinterComponent cmd: components)
		{
			cmd.stop();
		}
			
	}
}
