package org.bitbucket.winteruo.server.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.data.CityInfo;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.model.MobilesRepository;
import org.bitbucket.winteruo.api.model.UOItem;
import org.bitbucket.winteruo.api.model.UOMobile;
import org.bitbucket.winteruo.server.logger.WinterLogger;

public class World {

	private static WinterLogger logger = WinterLogger.getLogger(World.class);

	private static World instance_ = null;
	public static World getInstance() { if (instance_ == null) instance_ = new World(); return instance_;}

	public static List<CityInfo> citiesInfo = new ArrayList<CityInfo>();

	private static MobilesRepository mobiles = new MobilesRepository();
	private static HashMap<Integer, UOItem> items = new HashMap<Integer, UOItem>();


	private static final int MOBILES_MAX_SERIAL_ID = 0x3FFFFFFF;

	private static final int ITEMS_MAX_SERIAL_ID = MOBILES_MAX_SERIAL_ID + 1;
	private static final int OBJECTS_MAX_SERIAL_ID = 0x7FFFFFFF;


	private static int mobileLastSerialId;
	private static int itemLastSerialId;


	@Getter @Setter
	private ICore core;

	private Timer saveMobileTimer;

	public World() {


	}

	private TimerTask getSaveMobileTask()
	{
		return new TimerTask() {

			@Override
			public void run() {
				saveMobiles();

			}
		};
	}

	public void init()
	{
		loadMobiles();
		mobileLastSerialId = mobiles.getMobiles().size();
		//itemLastSerialId = 

		saveMobileTimer = new Timer();
		saveMobileTimer.schedule(getSaveMobileTask(), 0,  30 * 1000);

	}

	public UOMobile getMobileBySerialId(int serialId)
	{
		return mobiles.getMobiles().get(serialId);
	}

	public UOItem getItemBySerialId(int serialId)
	{
		return items.get(serialId);
	}

	public UOMobile generateNewMobile()
	{
		int serialId = mobileLastSerialId + 1;
		UOMobile mobile = new UOMobile();
		mobile.setSerialId(serialId);

		mobiles.getMobiles().put(serialId, mobile);
		mobileLastSerialId = serialId;

		saveMobiles();

		return getMobileBySerialId(serialId);

	}


	private void saveMobiles()
	{
		logger.info("Saving mobiles..");
		getCore().getConfigurationsManager().saveBinaryConfiguration(MobilesRepository.class, mobiles);
		logger.info("Mobiles saves: %s", mobiles.getMobiles().size() );

	}

	private void loadMobiles()
	{
		try
		{
			mobiles = (MobilesRepository) getCore().getConfigurationsManager().loadBinaryConfiguration(MobilesRepository.class);

			if (mobiles == null)
				mobiles = new MobilesRepository();

		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during load mobiles repository: %s", ex.getMessage());
		}
	}


	private boolean isMobile(int serialID) {
		return serialID > 0 && serialID <= MOBILES_MAX_SERIAL_ID;
	}

	private boolean isItem(int serialID) {
		return serialID >= ITEMS_MAX_SERIAL_ID && serialID <= OBJECTS_MAX_SERIAL_ID;
	}
}
