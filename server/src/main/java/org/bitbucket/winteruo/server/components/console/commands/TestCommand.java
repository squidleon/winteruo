package org.bitbucket.winteruo.server.components.console.commands;

import org.bitbucket.winteruo.api.annotations.ConsoleCmd;
import org.bitbucket.winteruo.api.annotations.ConsoleCmdRepository;
import org.bitbucket.winteruo.api.data.ConsoleInputCmd;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.network.ConsoleCommandLevel;


@ConsoleCmdRepository
public class TestCommand {

	
	@ConsoleCmd(cmd="test", level=ConsoleCommandLevel.ADMINISTRATOR)
	public String testCmd(ICore core, ConsoleInputCmd inputCmd)
	{
		
		
		return "1 2 3 ";
	}
}
