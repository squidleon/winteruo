package org.bitbucket.winteruo.server.components.plugins;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Set;

import org.bitbucket.winteruo.api.annotations.ConsoleCmd;
import org.bitbucket.winteruo.api.annotations.ConsoleCmdRepository;
import org.bitbucket.winteruo.api.annotations.WinterComponent;
import org.bitbucket.winteruo.api.annotations.utils.ScanAnnotations;
import org.bitbucket.winteruo.api.data.ConsoleInputCmd;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.IWinterComponent;
import org.bitbucket.winteruo.api.logger.IWinterLogger;

@WinterComponent(name=ConsoleComponent.CMP_NAME, version=ConsoleComponent.CMP_VERSION, description=ConsoleComponent.CMP_DESCR)
public class ConsoleComponent implements IWinterComponent {

	public static final String CMP_NAME = "Console";
	public static final String CMP_VERSION = "1.0";
	public static final String CMP_DESCR = "Handles CLI";


	private static final String CMD_PROMPT = ">";



	private HashMap<String, Method> registeredCommands = new HashMap<String, Method>();

	private IWinterLogger logger;
	private ICore core;
	private Thread thInput = null;
	private boolean canRun = true;

	public void setCore(ICore core) {
		this.core = core;

	}
	public void init() {


	}
	public void start() {
		logger.info("Starting...");
		scanForCommands();
		initInputThread();
	}
	public void stop() {
		try
		{
			canRun = false;
			thInput.join();
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during interruping command");
		}
	}

	private void initInputThread()
	{
		try
		{
			thInput = new Thread(new Runnable() {

				public void run() {

					String input = "";

					try
					{
						while(canRun)
						{
							BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

							input = bufferRead.readLine();

							if (input == null)
								input = "";

							if (!input.isEmpty())
							{
								ConsoleInputCmd inputCmd = parseInputCommand(input);



								logger.info("[%s]: %s", inputCmd.getCommand(), executeCommand(inputCmd));
							}

							System.out.print(CMD_PROMPT + "");
						}
					}
					catch(Exception ex)
					{
						logger.exception(ex,"Errore during executing command: '%s' => %s", input, ex.getMessage()  );
					}
				}
			});

			thInput.setName("ConsoleCommandLine parser");
			thInput.start();
		}
		catch(Exception ex)
		{

		}
	}


	public void registerCommand(String command, Method method)
	{
		registeredCommands.put(command, method);
	}

	private void scanForCommands()
	{
		try
		{
			//	Set<Method> classes =	ScanAnnotations.getAnnotatedMethodsFor(ConsoleCmd.class, "org.bitbucket.winteruo.server.component");
			Set<Class<?>> classes = ScanAnnotations.getAnnotationFor(ConsoleCmdRepository.class,"org.bitbucket.winteruo.server.component" );

			for(Class<?> classz: classes)
			{
				//				ConsoleCmd annotation = method.getAnnotation(ConsoleCmd.class);
			//	ConsoleCmdRepository annotation = classz.getAnnotation(ConsoleCmdRepository.class);

				for (Method method: classz.getDeclaredMethods())
				{
					if (method.getAnnotation(ConsoleCmd.class) != null)
					{
						ConsoleCmd consoleAnnotation = method.getAnnotation(ConsoleCmd.class);
						registerCommand(consoleAnnotation.cmd(), method);		
					}
				}
			}
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during scan console commands! ==> %s", ex.getMessage());
			System.exit(1);
		}
	}

	private ConsoleInputCmd parseInputCommand(String input)
	{

		String[] args = input.split(" ");

		ConsoleInputCmd cmd = new ConsoleInputCmd(args[0].toLowerCase());

		for (int i=1;i < args.length; i++)
		{
			cmd.addArg(args[i]);
		}

		return cmd;

	}

	private String executeCommand(ConsoleInputCmd cmd)
	{
		try
		{
			for (String command: registeredCommands.keySet() )
			{
				if (command.endsWith(cmd.getCommand()))
				{
					Method method = registeredCommands.get(command);
					
					method.setAccessible(true);
					Object obj = method.getDeclaringClass().newInstance();
					String result = (String) method.invoke(obj, core, cmd);

					return result;
				}
			}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return String.format("Error during execute command: %s ==> %s", cmd.getCommand(), ex.getMessage());
			
		}

		return String.format("Command %s not found please type 'help' to view all commands!", cmd.getCommand());

	}

	public void setLogger(IWinterLogger logger) {		
		this.logger = logger.newInstance(getClass());
	}



}
