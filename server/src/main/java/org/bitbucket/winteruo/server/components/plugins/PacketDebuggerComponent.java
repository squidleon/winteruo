package org.bitbucket.winteruo.server.components.plugins;

import java.io.File;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.bitbucket.winteruo.api.annotations.WinterComponent;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.IWinterComponent;
import org.bitbucket.winteruo.api.listeners.INetworkServerListener;
import org.bitbucket.winteruo.api.logger.IWinterLogger;
import org.bitbucket.winteruo.api.messages.AbstractMessage;

@WinterComponent(name=PacketDebuggerComponent.CMP_NAME, version=PacketDebuggerComponent.CMP_VERSION, description=PacketDebuggerComponent.CMP_DESCR)
public class PacketDebuggerComponent implements IWinterComponent, INetworkServerListener {

	public static final String CMP_NAME = "PacketsDebugger";
	public static final String CMP_VERSION = "1.0";
	public static final String CMP_DESCR = "Write incoming/outcoming packets";


	private IWinterLogger logger;
	private ICore core;

	private String packetsRoot;

	public void setCore(ICore core) {
		this.core = core;

	}


	public void setLogger(IWinterLogger logger) {
		this.logger = logger.newInstance(getClass());
	}


	public void init() {
		core.getNetworkServer().addNetworkListener(this);


		packetsRoot = core.getConfigurationsManager().getRootDirectory() + File.separator + "packets" + File.separator;

		if (!new File(packetsRoot).exists())
		{
			new File(packetsRoot).mkdirs();
		}
	}

	private void writeSendPackets(String session, byte[] buffer)
	{
		try
		{
			FileUtils.writeStringToFile(new File(packetsRoot + "session_" + session + ".log") , String.format("SERVER ==> CLIENT %s\n\r", formatBuffer(buffer)), true);
		}
		catch(Exception ex)
		{

		}
	}
	
	private void writeReceivedPackets(String session, byte[] buffer)
	{
		try
		{
			FileUtils.writeStringToFile(new File(packetsRoot + "session_" + session + ".log") , String.format("SERVER <== CLIENT  %s\n\r", formatBuffer(buffer)), true);
		}
		catch(Exception ex)
		{

		}
	}


	public void start() {
		// TODO Auto-generated method stub

	}


	public void stop() {
		// TODO Auto-generated method stub

	}


	public void onByteReceived(String sessionId, byte[] buffer) {
		writeReceivedPackets(sessionId, buffer);

	}

	public void onByteSended(String sessionId, byte[] buffer) {

		writeSendPackets(sessionId, buffer);

	}


	public void onMessageReceived(String sessionId, AbstractMessage message) {
		// TODO Auto-generated method stub

	}


	public void onMessageSended(String sessionId, AbstractMessage message) {
		// TODO Auto-generated method stub

	}

	private String formatBuffer(byte[] buffer)
	{
		String out = String.format("Length %s ==> ", buffer.length);
		
		for (int i=0; i<buffer.length;i++)
		{
			out += Hex.encodeHexString(new byte[] {buffer[i]}).toUpperCase() + ", ";
		}
		

		return out;
	}


}
