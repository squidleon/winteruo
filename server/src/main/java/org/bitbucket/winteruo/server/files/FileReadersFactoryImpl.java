package org.bitbucket.winteruo.server.files;

import java.io.File;
import java.io.FileNotFoundException;

import org.bitbucket.winteruo.api.files.FileReadersFactory;
import org.bitbucket.winteruo.api.files.IdxFileReader;
import org.bitbucket.winteruo.api.files.MapFileReader;
import org.bitbucket.winteruo.api.files.SkillsMulFileReader;

public class FileReadersFactoryImpl implements FileReadersFactory {
	
	public SkillsIdxFileReader createSkillsIdxFileReader(File file)
			throws FileNotFoundException {
		return new SkillsIdxFileReader(file);
	}
	

	
	public MapFileReader createMapFileReader(File mapFile,
			int mapHeight) throws FileNotFoundException {
		return new MapFileReaderImpl(mapFile, mapHeight);
	}
	
	
	public SkillsMulFileReader createSkillsMulFileReader(File file,
			IdxFileReader idxFileReader) throws FileNotFoundException {
		return new SkillsMulFileReaderImpl(file, idxFileReader);
	}
}
