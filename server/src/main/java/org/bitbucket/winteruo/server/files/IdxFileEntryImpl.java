package org.bitbucket.winteruo.server.files;

import org.bitbucket.winteruo.api.files.IdxFileEntry;

public class IdxFileEntryImpl implements IdxFileEntry {
	/**
	 * 0-based entry index.
	 */
	private int index;
	/**
	 * Starting position within the data file.
	 */
	private int start;
	/**
	 * Length of the portion within the data file.
	 */
	private int length;

	public IdxFileEntryImpl(int start, int length) {
		super();
		this.start = start;
		this.length = length;
	}

	
	public IdxFileEntry index(int index) {
		this.index = index;
		return this;
	}

	
	public int getIndex() {
		return index;
	}

	
	public int getStart() {
		return start;
	}

	
	public int getLength() {
		return length;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdxFileEntryImpl other = (IdxFileEntryImpl) obj;
		if (index != other.index)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IdxFileEntryImpl [index=" + index + ", start=" + start
				+ ", length=" + length + "]";
	}
}
