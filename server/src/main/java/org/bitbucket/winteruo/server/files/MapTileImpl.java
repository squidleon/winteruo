package org.bitbucket.winteruo.server.files;

import org.bitbucket.winteruo.api.model.MapTile;

public class MapTileImpl implements MapTile {
	private int z;
	private int tileID;

	public MapTileImpl(int z, int color) {
		super();
		this.z = z;
		this.tileID = color;
	}

	
	public int getZ() {
		return z;
	}

	
	public int getTileID() {
		return tileID;
	}

	@Override
	public String toString() {
		return "MapTileImpl [z=" + z + ", tileID=" + tileID + "]";
	}
}
