package org.bitbucket.winteruo.server.files;

import org.bitbucket.winteruo.api.files.SkillsFileEntry;

public class SkillsFileEntryImpl implements SkillsFileEntry {
	private String skillName;
	private boolean hasButton;

	public SkillsFileEntryImpl(String skillName, boolean hasButton) {
		super();
		this.skillName = skillName;
		this.hasButton = hasButton;
	}

	
	public String getSkillName() {
		return skillName;
	}

	
	public boolean isHasButton() {
		return hasButton;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((skillName == null) ? 0 : skillName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SkillsFileEntryImpl other = (SkillsFileEntryImpl) obj;
		if (skillName == null) {
			if (other.skillName != null)
				return false;
		} else if (!skillName.equals(other.skillName))
			return false;
		return true;
	}
}
