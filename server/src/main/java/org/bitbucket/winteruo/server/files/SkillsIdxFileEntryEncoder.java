package org.bitbucket.winteruo.server.files;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.bitbucket.winteruo.api.files.IdxFileEntry;
import org.bitbucket.winteruo.api.files.IdxFileEntryEncoder;

/**
 * Encoder for the skills index file (namely, <tt>skills.idx</tt>).
 */
public class SkillsIdxFileEntryEncoder implements IdxFileEntryEncoder {

	public IdxFileEntry encode(byte[] contents) {
		ByteBuffer bb = ByteBuffer.wrap(contents).order(ByteOrder.LITTLE_ENDIAN);
		int start = bb.getInt();
		int length = bb.getInt();
		return new IdxFileEntryImpl(start, length);
	}
}
