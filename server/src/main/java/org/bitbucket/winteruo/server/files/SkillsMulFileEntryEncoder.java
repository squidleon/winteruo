package org.bitbucket.winteruo.server.files;

import java.util.Arrays;

import org.bitbucket.winteruo.api.files.GenericFileEntryEncoder;
import org.bitbucket.winteruo.api.files.SkillsFileEntry;

/**
 * Encoder for the skills data file (namely, <tt>skills.mul</tt>).
 */
public class SkillsMulFileEntryEncoder implements GenericFileEntryEncoder<SkillsFileEntry> {

	
	public SkillsFileEntry encode(byte[] contents) {
		// First byte is 1 if there is a button next to the name,
		// 0 if there is not. Last byte is always zero.
		return new SkillsFileEntryImpl(
				new String(Arrays.copyOfRange(contents, 1, contents.length - 1)),
				contents[0] == 1);
	}
}
