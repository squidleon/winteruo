package org.bitbucket.winteruo.server.files;

import java.io.File;
import java.io.FileNotFoundException;

import org.bitbucket.winteruo.api.files.IdxFileReader;
import org.bitbucket.winteruo.api.files.SkillsFileEntry;
import org.bitbucket.winteruo.api.files.SkillsMulFileReader;

class SkillsMulFileReaderImpl extends MulFileReader<SkillsFileEntry>
		implements SkillsMulFileReader {
	
	public SkillsMulFileReaderImpl(File file, IdxFileReader idxFileReader)
			throws FileNotFoundException {
		super(file, idxFileReader, new SkillsMulFileEntryEncoder());
	}

	
}
