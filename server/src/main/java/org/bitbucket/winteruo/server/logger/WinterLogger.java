package org.bitbucket.winteruo.server.logger;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bitbucket.winteruo.api.logger.IWinterLogger;



/**
 * Logger class who bind Log4J logging
 * You have also <b>exception(..)</b> method for handle exceptions
 * @author Squid <squid@stormwind.it>
 * @param <T> Class to log
 *
 */
public class WinterLogger implements IWinterLogger {

	/**
	 * Get new instance of WinterLogger
	 * @param classz
	 * @return
	 */
	public static WinterLogger getLogger(Class<?> classz) 
	{
		return new WinterLogger(classz);
	}

	private Class<?> classz;
	private Logger logger;

	public WinterLogger(Class<?> classz) {

		this.classz = classz;
		this.logger = Logger.getLogger(this.classz);
	}

	public IWinterLogger newInstance(Class<?> classz) {
		return WinterLogger.getLogger(classz);
	}
	
	public void info(String text, Object ... args)
	{
		log(Level.INFO, text, args);
	}

	public void warn(String text, Object  ... args)
	{
		log(Level.WARN, text, args);
	}

	public void debug(String text, Object  ... args)
	{
		log(Level.DEBUG, text, args);
	}

	public void error(String text, Object ... args)
	{
		log(Level.FATAL, text, args);
	}

	public void exception(Exception ex, String text, Object ... args)
	{
		error("BEGIN EXCEPTION ------------------");
		error("%s has generated an error:", classz.getName() );
		error("---------------------------");
		error("Text: %s", String.format(text, args) );
		error("Exception: %s", ex.getMessage() );
		error("Print Stack: %s", ExceptionUtils.getStackTrace(ex.fillInStackTrace()));
		error("END EXCEPTION ------------------");			
	}

	public void log(Level level, String text, Object ... args)
	{
		logger.log(level, String.format(text, args));
	}


}
