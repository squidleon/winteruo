package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.WarmodeFlag;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=CharacterWarmodeMessage.CODE, isCompress=true)
public class CharacterWarmodeMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte)0x72;
	public static final int LENGTH = 5;


	private WarmodeFlag warmode;

	public CharacterWarmodeMessage(WarmodeFlag warmode) {
		super(CODE);
		this.warmode = warmode;
	}


	public CharacterWarmodeMessage() {
		super(CODE);
	}

	@Override
	public int getLength() {

		return LENGTH;
	}


	@Override
	public void parse(PacketReader reader) {
		boolean isWarmode = reader.ReadBoolean();

		if (isWarmode)
			warmode = WarmodeFlag.WAR;
		else
			warmode = WarmodeFlag.PEACE;
	}

	@Override
	public byte[] getBytes() {

		ByteBuffer buffer = ByteBuffer.allocate(getLength());
		buffer.put(CODE);
		buffer.put((byte)warmode.ordinal());
		buffer.put((byte)0);
		buffer.put((byte)0);
		buffer.put((byte)0);


		return buffer.array();


	}



}
