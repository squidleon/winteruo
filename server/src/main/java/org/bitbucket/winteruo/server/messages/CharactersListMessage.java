package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.data.CityInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.CharacterListFlags;
import org.bitbucket.winteruo.api.model.Account;
import org.bitbucket.winteruo.api.model.AccountCharacter;
import org.bitbucket.winteruo.api.model.Flag;
import org.bitbucket.winteruo.api.model.UOMobile;
import org.bitbucket.winteruo.api.utils.MessagesUtils;
import org.bitbucket.winteruo.server.components.World;


@PacketInfo(code=CharactersListMessage.CODE, isCompress=true)
public class CharactersListMessage  extends AbstractMessage {


	private static final long serialVersionUID = 1L;
	public static final byte CODE = (byte)0xA9;
	public static final int LENGTH = 0;


	private List<CityInfo> cities = new ArrayList<CityInfo>();
	private List<AccountCharacter> characters = new ArrayList<AccountCharacter>();
	private Flag[] flags;


	public void addCity(CityInfo city)
	{
		cities.add(city);
	}

	public void addCities(List<CityInfo> cities)
	{
		this.cities = cities;
	}

	public void addFlags(Flag... flags)
	{
		this.flags = flags;
	}


	public CharactersListMessage(Account account) {
		super(CODE);
		characters = account.getCharacters();
	}


	@Override
	public byte[] getBytes() {
		ByteBuffer bb = ByteBuffer.allocate( 11 + (7 * 60)  + (cities.size() * 89));

		bb.put(CODE);
		bb.putShort((short) bb.capacity());
		bb.put((byte) 7);

		for (int i=0;i<7;i++)
		{
			if (characters.size() > i)
			{
				UOMobile mobile = World.getInstance().getMobileBySerialId(characters.get(i).getMobileID());
				
				bb.put( MessagesUtils.padString(mobile.getName(), 30) );
				bb.put( MessagesUtils.padString("", 30) );
			}
			else
			{
				bb.put( MessagesUtils.padString("", 60) );
			}

		}
	
		bb.put((byte) cities.size());
		int i = -1;
		for (CityInfo city : cities) {
			bb.put((byte) i++);
			bb.put( MessagesUtils.padString(city.getCity(), 32) );
			bb.put( MessagesUtils.padString(city.getBuilding(), 32) );
			bb.putInt(city.getLocation().getX());
			bb.putInt(city.getLocation().getY());
			bb.putInt(city.getLocation().getX());
			bb.putInt(city.getMapId());
			bb.putInt(city.getDescription());
			bb.putInt(0);

		}

		int fCurrents = CharacterListFlags.ContextMenus | CharacterListFlags.AOS | CharacterListFlags.SE | CharacterListFlags.ML;

		fCurrents |= (CharacterListFlags.SeventhCharacterSlot | CharacterListFlags.SixthCharacterSlot);
		
	
		//for (Flag f : flags) {
		//	fCurrents |= f.getValue();
		//}
		bb.putInt(fCurrents);
		bb.putShort((short)-1);
		
		return bb.array();
	}



}
