package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.VersionInfo;
import org.bitbucket.winteruo.api.utils.PacketReader;


@PacketInfo(code=ClientVersionMessage.CODE, isCompress=false)
public class ClientVersionMessage extends AbstractMessage  {

	private static final long serialVersionUID = 1L;
	
	public static final byte CODE = (byte)0xBD;

	private VersionInfo version;
	
	public ClientVersionMessage() {
		super(CODE);	
	}
	
	public ClientVersionMessage(VersionInfo version) {
		super(CODE);	
		this.version = version;
	}
	
	
	@Override
	public void parse(PacketReader reader) {
		int length = reader.ReadInt16();
		String version = reader.ReadString(length);
		
	}
	
	
	@Override
	public byte[] getBytes() {
	
		ByteBuffer buffer = ByteBuffer.allocate(3);
		buffer.put(CODE);
		buffer.putShort((short) 3);
		return buffer.array();
	}
	
	
	
	
	

}
