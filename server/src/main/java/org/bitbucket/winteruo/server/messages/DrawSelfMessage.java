package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.AccountCharacter;
import org.bitbucket.winteruo.api.model.UOMobile;
import org.bitbucket.winteruo.api.network.PackInfoDirection;
import org.bitbucket.winteruo.server.components.World;


@PacketInfo(code=DrawSelfMessage.CODE, isCompress=true, direction=PackInfoDirection.FROM_SERVER)
public class DrawSelfMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte)0x19;
	public static final int LENGHT = 20;

	@Getter @Setter
	private AccountCharacter character;

	public DrawSelfMessage(AccountCharacter character) {
		super(CODE);
		this.character = character;
	}


	@Override
	public int getLength() {

		return LENGHT;

	}

	@Override
	public byte[] getBytes() {
		
	 UOMobile mobile=	World.getInstance().getMobileBySerialId(character.getMobileID());
		ByteBuffer buffer = ByteBuffer.allocate(getLength());
		buffer.put(CODE);
		buffer.putInt(character.getMobileID());
		buffer.putShort((short) mobile.getModelId());
		buffer.put((byte)0);
		buffer.putShort((short)mobile.getSkinColor());
		buffer.put((byte)0);
		buffer.putShort((short)mobile.getX());
		buffer.putShort((short)mobile.getY());
		buffer.putShort((short)0);
		buffer.put((byte)mobile.getDirection().ordinal());
		buffer.put((byte)mobile.getZ());
		
		
		return buffer.array();

	}







}
