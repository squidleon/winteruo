package org.bitbucket.winteruo.server.messages;

import java.net.InetAddress;
import java.nio.ByteBuffer;

import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.ServerInfo;
import org.bitbucket.winteruo.api.utils.MessagesUtils;

public class GameServerEntryMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;
	public static final byte CODE = (byte)0x00;
	public static final int LENGTH = 40;

	private short index;
	private String serverName;
	private InetAddress address;

	public GameServerEntryMessage(short index, ServerInfo serverInfo) {
		super(CODE);

		try
		{
			this.index = index;
			this.serverName = serverInfo.getServerName();
			this.address = serverInfo.getAddress();
		}
		catch(Exception ex)
		{

		}
	}

	@Override
	public int getLength() {
		return LENGTH;
	}

	@Override
	public byte[] getBytes() {

		ByteBuffer pw = ByteBuffer.allocate(getLength());
		pw.putShort(index);
		pw.put(MessagesUtils.padString(serverName, 32));
		pw.put((byte)0x00);
		pw.put((byte)0x00);
		pw.put(address.getAddress());

		return pw.array();

	}




}
