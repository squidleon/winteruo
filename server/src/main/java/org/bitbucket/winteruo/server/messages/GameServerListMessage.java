package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bitbucket.winteruo.api.messages.AbstractMessage;

public class GameServerListMessage extends AbstractMessage {


	private static final long serialVersionUID = 1L;
	public static final byte CODE = (byte)0xA8;

	private List<GameServerEntryMessage> gameServers = new ArrayList<GameServerEntryMessage>();

	public GameServerListMessage(GameServerEntryMessage ... servers) {
		super(CODE);
		
		this.gameServers = Arrays.asList(servers);
	}

	@Override
	public int getLength() {
		return 6 + (40 * gameServers.size());
	}

	@Override
	public byte[] getBytes() {

		ByteBuffer pw = ByteBuffer.allocate(getLength());
		pw.put(CODE);
		pw.putShort((short)getLength());
		pw.put((byte)0xCC);
		//pw.put((byte)0x5D); // Send video card info
		pw.putShort((short)gameServers.size());

		for (GameServerEntryMessage entry: gameServers)
		{
			pw.put(entry.getBytes());
		}

		return pw.array();

	}


}
