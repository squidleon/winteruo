package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import lombok.Getter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=GameServerLoginMessage.CODE)
public class GameServerLoginMessage extends AbstractMessage {


	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte) 0x91;
	public static final int LENGTH = 65;

	@Getter
	private int authId;

	@Getter
	private String username;
	@Getter
	private String password;


	public GameServerLoginMessage() {
		super(CODE);

	}

	@Override
	public int getLength() {
		return LENGTH;
	}

	@Override
	public void parse(PacketReader reader) {
		ByteBuffer bf =  ByteBuffer.wrap(reader.getBytes(),1,4);
		authId = bf.getInt();
		reader.ReadInt32();
		username = reader.ReadString(30);
		password = reader.ReadString(30);

	}

	@Override
	public String toString() {
		return String.format("AuthID=%s Username=%s Password=%s",authId, username, password);
	}


}
