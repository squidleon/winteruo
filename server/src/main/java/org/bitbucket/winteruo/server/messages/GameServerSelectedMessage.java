package org.bitbucket.winteruo.server.messages;

import lombok.Getter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=GameServerSelectedMessage.CODE)
public class GameServerSelectedMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte) 0xA0;
	public static final int LENGTH = 3;

	@Getter
	private short seletectedServer;

	public GameServerSelectedMessage() {
		super(CODE);

	}

	@Override
	public int getLength() {
		return LENGTH;

	}

	@Override
	public void parse(PacketReader reader) {
	
		seletectedServer = reader.ReadInt16();
	}
	
	@Override
	public String toString() {
		return String.format("Selected game server: %s", seletectedServer);
	}


}
