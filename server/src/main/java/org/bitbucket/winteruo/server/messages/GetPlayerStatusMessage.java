package org.bitbucket.winteruo.server.messages;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.GetPlayerStatusType;
import org.bitbucket.winteruo.api.network.PackInfoDirection;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=GetPlayerStatusMessage.CODE, direction=PackInfoDirection.FROM_CLIENT)
public class GetPlayerStatusMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;
	
	public static final byte CODE = (byte)0x34;
	public static final int LENGTH = 10;
	
	
	@Getter @Setter
	private GetPlayerStatusType type;
	@Getter @Setter
	private int serialId;
	
	public GetPlayerStatusMessage() {
		super(CODE);
		
	}

	@Override
	public int getLength() {
		return LENGTH;
	}
	
	
	@Override
	public void parse(PacketReader reader) 
	{
	
		reader.ReadInt32();
		type = GetPlayerStatusType.getType(reader.ReadByte());
		serialId = reader.ReadInt32();
	
	}
	
	
	@Override
	public String toString() {
		return String.format("Request %s serialId: %s", getType(), serialId);
	}
}
