package org.bitbucket.winteruo.server.messages;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=LoginCharacterMessage.CODE, isCompress=true)
public class LoginCharacterMessage  extends AbstractMessage {


	public static final byte CODE = 0x5D;
	public static final int LENGHT = 73;
	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private String characterName;

	@Getter @Setter
	private int flags;

	@Getter @Setter
	private int loginCount;

	@Getter @Setter
	private int slotSelected;


	public LoginCharacterMessage() {
		super(CODE);
	}


	@Override
	public void parse(PacketReader reader) {

		//(0xedededed)
		reader.ReadInt32();

		setCharacterName(reader.ReadString(30));
		reader.ReadInt16();
		setFlags(reader.ReadInt32());
		reader.ReadInt32();
		setLoginCount(reader.ReadInt32());
		reader.ReadInt32();
		reader.ReadInt32();
		setSlotSelected(reader.ReadInt32());
	}


	@Override
	public String toString() {

		return String.format("Selected character name: \"%s\" slotNumber: %s", getCharacterName(), getSlotSelected());
	}



}
