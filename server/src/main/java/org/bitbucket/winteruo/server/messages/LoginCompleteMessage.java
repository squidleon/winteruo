package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.network.PackInfoDirection;

@PacketInfo(code=LoginCompleteMessage.CODE, direction=PackInfoDirection.FROM_SERVER, isCompress=true)
public class LoginCompleteMessage extends AbstractMessage {
	
	private static final long serialVersionUID = 1L;
	
	public static final byte CODE = (byte)0x78;
	public static final int LENGHT = 1;
	
	public LoginCompleteMessage() {
		super(CODE);
		
	}
	
	@Override
	public byte[] getBytes() {
		ByteBuffer buffer =ByteBuffer.allocate(LENGHT);
		buffer.put(CODE);
		
		return buffer.array();


	}
	

}
