package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.Direction;
import org.bitbucket.winteruo.api.model.Point2D;
import org.bitbucket.winteruo.api.model.Point3D;

@PacketInfo(code=LoginConfirmationMessage.CODE, isCompress=true)
public class LoginConfirmationMessage extends AbstractMessage {


	private static final long serialVersionUID = 1L;
	public static final byte CODE = 0x1B;
	public static final int LENGTH = 37;

	@Getter @Setter
	private int characterSerialID;

	@Getter @Setter
	private short bodyType;

	@Getter @Setter
	private Point3D location;

	@Getter @Setter
	private Direction direction;

	@Getter @Setter
	private Point2D mapSize;

	public LoginConfirmationMessage(int charSerialID, short bodyType, Point3D location, Direction direction, Point2D mapSize) {
		super(CODE);
		this.characterSerialID = charSerialID;
		this.bodyType = bodyType;
		this.location = location;
		this.direction = direction;
		this.mapSize = mapSize;

		if (this.mapSize == null)
			this.mapSize = new Point2D(6144, 4096);

	}


	@Override
	public byte[] getBytes() {

		ByteBuffer buffer = ByteBuffer.allocate(LENGTH);
		buffer.put(CODE);
		buffer.putInt(getCharacterSerialID());
		buffer.putInt(0);
		buffer.putShort(getBodyType());
		buffer.putShort((short)getLocation().getX());
		buffer.putShort((short)getLocation().getY());
		buffer.putShort((short)getLocation().getZ());
		buffer.put((byte)getDirection().ordinal());
		buffer.put((byte)0);
		buffer.putInt(-1);
		buffer.putShort((short)0);
		buffer.putShort((short)0);
		buffer.putShort((short)getMapSize().getX());
		buffer.putShort((short)getMapSize().getY());

		return buffer.array();
	}



}
