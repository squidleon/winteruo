package org.bitbucket.winteruo.server.messages;

import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.PacketWriter;
import org.bitbucket.winteruo.server.messages.enums.LoginDeniedType;

public class LoginDeniedMessage extends AbstractMessage {

	public static final byte CODE = (byte) 0x82;
	public static final int SIZE = 2;

	private static final long serialVersionUID = 1L;
	
	private LoginDeniedType deniedType;

	public LoginDeniedMessage(LoginDeniedType deniedType) {
		super(CODE);
		this.deniedType = deniedType;
	}

	@Override
	public int getLength() {
		return SIZE;
	}
	
	
	@Override
	public byte[] getBytes() {
		PacketWriter pckWriter = new PacketWriter(getLength());
		pckWriter.Write(getCode());		
		pckWriter.Write( (byte) deniedType.getIndex());


		return pckWriter.getBytes();
	}

}
