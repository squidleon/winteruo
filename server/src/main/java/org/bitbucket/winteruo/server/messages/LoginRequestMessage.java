package org.bitbucket.winteruo.server.messages;

import lombok.Getter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=LoginRequestMessage.CODE)
public class LoginRequestMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte) 0x80;
	public static final int LENGTH = 62;

	@Getter
	private String username;
	@Getter
	private String password;
	@Getter
	private byte nextLoginKey;


	public LoginRequestMessage() {
		super(CODE);
	}

	@Override
	public void parse(PacketReader reader) {

		username = reader.ReadString(30);
		password = reader.ReadString(30);
		nextLoginKey = reader.ReadByte();
	}
	
	@Override
	public String toString() {
	
		return String.format("Request username: %s password: %s netLoginKey: 0x%02X", username, password, nextLoginKey);
	}
}
