package org.bitbucket.winteruo.server.messages;

import lombok.Getter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=LoginSeedMessage.CODE)
public class LoginSeedMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte) 0xEF;
	public static final int LENGTH = 21;

	private int seed;

	@Getter
	private int clientMaj;
	@Getter
	private int clientMin;
	@Getter
	private int clientRev;
	@Getter
	private int clientPat;




	public LoginSeedMessage() {
		super(CODE);

	}

	@Override
	public int getLength() {
		return LENGTH;
	}

	@Override
	public void parse(PacketReader reader) {

		seed = reader.ReadInt32();
		
		clientMaj = reader.ReadInt32();
		clientMin = reader.ReadInt32();
		clientRev = reader.ReadInt32();
		clientPat = reader.ReadInt32();
	}

	
	@Override
	public String toString() {
		return String.format("Seed=%s -- Client version v%s.%s.%s.%s", seed, clientMaj, clientMin, clientRev, clientPat);
	}
}
