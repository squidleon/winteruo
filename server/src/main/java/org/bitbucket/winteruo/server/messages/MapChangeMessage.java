package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.MapFileInfo;
import org.bitbucket.winteruo.api.network.PackInfoDirection;

@PacketInfo(code=MapChangeMessage.CODE, isCompress=true, direction=PackInfoDirection.FROM_SERVER)
public class MapChangeMessage extends AbstractMessage {
	
	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte)0xBF;
	public static final int LENGHT = 6;
	
	private MapFileInfo mapInfo;
	
	public MapChangeMessage(MapFileInfo mapInfo) {
		super(CODE);
		this.mapInfo = mapInfo;
	}

	
	@Override
	public int getLength() {
	
		return LENGHT;
	}
	
	@Override
	public byte[] getBytes() {
		ByteBuffer buffer = ByteBuffer.allocate(getLength());
		buffer.put(CODE);
		buffer.putShort((short)0x08);
		buffer.put((byte)mapInfo.getMapId());
		
		
		return buffer.array();
	}
}
