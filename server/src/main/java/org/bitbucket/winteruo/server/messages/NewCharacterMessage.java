package org.bitbucket.winteruo.server.messages;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.SexType;
import org.bitbucket.winteruo.api.model.AccountCharacter;
import org.bitbucket.winteruo.api.model.SkillGame;
import org.bitbucket.winteruo.api.model.SkillInfo;
import org.bitbucket.winteruo.api.model.SkillLockFlag;
import org.bitbucket.winteruo.api.model.UOMobile;
import org.bitbucket.winteruo.api.utils.PacketReader;
import org.bitbucket.winteruo.server.components.World;


@PacketInfo(code=NewCharacterMessage.CODE)
public class NewCharacterMessage extends AbstractMessage {


	private static final long serialVersionUID = 1L;
	public static final byte CODE = (byte)0xF8;
	public static final int LENGTH = 106;
	
	
	@Getter @Setter
	private AccountCharacter newCharacter;
	

	public NewCharacterMessage() {
		super(CODE);

	}

	@Override
	public int getLength() {
		return LENGTH;
	}

	
	@Override
	public void parse(PacketReader reader) {
			
		newCharacter = new AccountCharacter();
		
		UOMobile mobile =  World.getInstance().generateNewMobile();
		
		newCharacter.setMobileID(mobile.getSerialId());
		
		reader.ReadInt32();
		reader.ReadInt32();
		reader.ReadByte();
		
		mobile.setName(reader.ReadString(30).trim());
		//newCharacter.setName();
		
		reader.ReadByte();
		reader.ReadByte();

		//newCharacter.setClientFlag(reader.ReadInt32());
		reader.ReadInt32();
		
		reader.ReadInt32();
		reader.ReadInt32();
		
		//newCharacter.setProfession(reader.ReadByte());
		reader.ReadByte();
		
		reader.ReadInt32();
		reader.ReadInt32();
		reader.ReadInt32();
		reader.ReadByte();
		reader.ReadByte();
		reader.ReadByte();

		mobile.setSexRace(SexType.getValue(Integer.valueOf(reader.ReadByte())));
		
		mobile.setStrength(Integer.valueOf(reader.ReadByte()));
		mobile.setDexterity(Integer.valueOf(reader.ReadByte()));
		mobile.setIntelligence(Integer.valueOf(reader.ReadByte()));
		
		addSkillToCharacter(mobile.getSerialId(), Integer.valueOf(reader.ReadByte()), Integer.valueOf(reader.ReadByte()));
		addSkillToCharacter(mobile.getSerialId(), Integer.valueOf(reader.ReadByte()), Integer.valueOf(reader.ReadByte()));
		addSkillToCharacter(mobile.getSerialId(), Integer.valueOf(reader.ReadByte()), Integer.valueOf(reader.ReadByte()));
		addSkillToCharacter(mobile.getSerialId(), Integer.valueOf(reader.ReadByte()), Integer.valueOf(reader.ReadByte()));
		
		mobile.setSkinColor(reader.ReadInt16());
		mobile.setHairStyle(reader.ReadInt16());
		mobile.setHairColor(reader.ReadInt16());
		mobile.setFacialHair(reader.ReadInt16());
		mobile.setFaciaHairColor(reader.ReadInt16());
			
		newCharacter.setStartupLocationId(reader.ReadInt16());
		
		reader.ReadInt16();
		reader.ReadInt16();
		reader.ReadInt32();
		reader.ReadInt32();
		
		mobile.setShirtColor(reader.ReadInt16());
		mobile.setPantsColor(reader.ReadInt16());
		
		
		
		
		
		
	}
	
	private void addSkillToCharacter(int serialId, int id, int value)
	{
		if (id != -1)
		{ 
			SkillInfo info = new SkillInfo(id, "");
			World.getInstance().getMobileBySerialId(serialId).getSkills().add(new SkillGame(info, value, 1, 100, SkillLockFlag.Up));

		}
	}


}
