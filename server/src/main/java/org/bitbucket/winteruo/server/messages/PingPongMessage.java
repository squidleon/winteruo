package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.PacketReader;

@PacketInfo(code=PingPongMessage.CODE)
public class PingPongMessage extends AbstractMessage {


	public static final byte CODE = (byte) 0x73;
	public static final int SIZE = 2;


	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private byte sequenceNumber = (byte)0x00;

	public PingPongMessage() {
		super(CODE);
	}

	@Override
	public void parse(PacketReader reader) {
		sequenceNumber = reader.ReadByte();
	}

	@Override
	public byte[] getBytes() {
		ByteBuffer buffer = ByteBuffer.allocate(getLength());
		buffer.put(CODE);
		buffer.put(sequenceNumber);
		
		return buffer.array();
	}

	@Override
	public int getLength() {
		return SIZE;
	}


}
