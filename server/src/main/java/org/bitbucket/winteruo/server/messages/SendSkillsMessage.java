package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Set;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.SendSkillsType;
import org.bitbucket.winteruo.api.model.SkillGame;
import org.bitbucket.winteruo.api.network.PackInfoDirection;

@PacketInfo(code=SendSkillsMessage.CODE, direction=PackInfoDirection.BOTH, isCompress=true)
public class SendSkillsMessage extends AbstractMessage {
	
	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte)0x3A;

	private SendSkillsType type;
	private Set<SkillGame> skillsToSend;
	
	
	public SendSkillsMessage() {
		super(CODE);
		
	}
	
	public SendSkillsMessage(SendSkillsType type, Set<SkillGame> skillsToSend) {
		super(CODE);
		this.type = type;
		this.skillsToSend = skillsToSend;
	
	}
	
	@Override
	public byte[] getBytes() {
		ByteBuffer buffer = ByteBuffer.allocate(6 + ( skillsToSend.size() * 9));
		buffer.put(CODE);
		buffer.putShort((short)(6 + ( skillsToSend.size() * 9)));
		buffer.put((byte)SendSkillsType.FULL_LIST_WITH_CAP.ordinal());
		
		for (SkillGame skill : skillsToSend)
		{
			buffer.putShort((short)skill.getBaseSkill().getSkillId());
			buffer.putShort((short)(skill.getValue() * 10));
			buffer.putShort((short)(skill.getUnmodifiedValue() * 10));
			buffer.put((byte)skill.getLockFlag().ordinal());
			buffer.putShort((short)(skill.getValue()));
			
						
		}
		
		buffer.putShort((short)0x00);
		
		return buffer.array();
	}
	
	
	
	
	

}
