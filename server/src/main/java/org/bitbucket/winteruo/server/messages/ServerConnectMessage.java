package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;

import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.ServerInfo;

public class ServerConnectMessage extends AbstractMessage {


	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte) 0x8C;
	public static final int LENGTH = 11;

	private ServerInfo serverInfo;
	private int authId;


	public ServerConnectMessage(ServerInfo serverInfo, int authId) {
		super(CODE);
		this.serverInfo = serverInfo;
		this.authId = authId;
	}

	@Override
	public byte[] getBytes() {
		ByteBuffer pw = ByteBuffer.allocate(getLength());
		pw.put(CODE);
		pw.put(serverInfo.getAddress().getAddress());
		pw.putShort((short)serverInfo.getPort());
		pw.putInt(authId);
		
		return pw.array();
	}


	@Override
	public int getLength() {
		return LENGTH;
	}


}
