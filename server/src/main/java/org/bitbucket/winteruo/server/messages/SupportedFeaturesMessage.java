package org.bitbucket.winteruo.server.messages;

import java.nio.ByteBuffer;
import java.util.BitSet;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.FeatureFlags;

@PacketInfo(code=SupportedFeaturesMessage.CODE, isCompress=true)
public class SupportedFeaturesMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte)0xB9;

	public static final int LENGTH = 5;

	public SupportedFeaturesMessage() {
		super(CODE);

	}

	@Override
	public int getLength() {
		return LENGTH;
	}

	@Override
	public byte[] getBytes() {
		int flags = FeatureFlags.T2A | FeatureFlags.UOR | FeatureFlags.UOTD | FeatureFlags.LBR | FeatureFlags.AOS | FeatureFlags.SE | FeatureFlags.ML | FeatureFlags.Unk2 | FeatureFlags.Unk7  | FeatureFlags.SA | FeatureFlags.Gothic | FeatureFlags.Rustic;

	
		flags |= FeatureFlags.ExpansionML; 
		flags |= FeatureFlags.Unk7;
		flags &= ~FeatureFlags.UOTD;

		
		flags |= FeatureFlags.SeventhCharacterSlot;

		ByteBuffer buffer = ByteBuffer.allocate(LENGTH);
		buffer.put(CODE);
		
		buffer.putInt((int)flags);
		

		return buffer.array();
	}



}
