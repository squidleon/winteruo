package org.bitbucket.winteruo.server.messages.enums;

public enum LoginDeniedType {


	IncorrectUsername,
	SomeoneUsingThisAccount,
	AccountBlocked,
	CredentialInvalid,
	CommunicationProblem,
	IGRLimit;


	public int getIndex() { return ordinal();  }  



}
