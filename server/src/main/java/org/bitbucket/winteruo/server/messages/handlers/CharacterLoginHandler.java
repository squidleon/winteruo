package org.bitbucket.winteruo.server.messages.handlers;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.WarmodeFlag;
import org.bitbucket.winteruo.api.model.Point3D;
import org.bitbucket.winteruo.api.model.UOMobile;
import org.bitbucket.winteruo.server.components.World;
import org.bitbucket.winteruo.server.messages.CharacterWarmodeMessage;
import org.bitbucket.winteruo.server.messages.DrawSelfMessage;
import org.bitbucket.winteruo.server.messages.LoginCharacterMessage;
import org.bitbucket.winteruo.server.messages.LoginCompleteMessage;
import org.bitbucket.winteruo.server.messages.LoginConfirmationMessage;
import org.bitbucket.winteruo.server.messages.MapChangeMessage;
import org.bitbucket.winteruo.server.messages.SupportedFeaturesMessage;

public class CharacterLoginHandler implements IGameMessageListener {

	public void onMessageReceived(AbstractMessage message, INetState netState,
			ICore core) {

		LoginCharacterMessage characterMsg = (LoginCharacterMessage)message;

		UOMobile mobile = World.getInstance().getMobileBySerialId(netState.getAccount().getCharacters().get(characterMsg.getSlotSelected()).getMobileID());

	

		netState.setCurrentCharacter(netState.getAccount().getCharacters().get(characterMsg.getSlotSelected()));

		netState.sendMessage(new LoginConfirmationMessage(mobile.getSerialId(), (short)mobile.getModelId(), new Point3D(mobile.getX(), mobile.getY(), mobile.getZ()), mobile.getDirection(), null));
		netState.sendMessage(new SupportedFeaturesMessage());
		netState.sendMessage(new CharacterWarmodeMessage(WarmodeFlag.PEACE));
		//netState.sendMessage(new ClientVersionMessage(netState.getVersionInfo()));
		netState.sendMessage(new MapChangeMessage(core.getFileManager().getMapFileInfoById(0)));
		//netState.sendMessage(new SeasonChangeMessage(SeasonFlag.SUMMER, true));
		netState.sendMessage(new DrawSelfMessage(netState.getCurrentCharacter()));
		netState.sendMessage(new LoginCompleteMessage());
		

	}



}
