package org.bitbucket.winteruo.server.messages.handlers;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.Flag;
import org.bitbucket.winteruo.server.messages.CharactersListMessage;
import org.bitbucket.winteruo.server.messages.ClientVersionMessage;
import org.bitbucket.winteruo.server.messages.GameServerLoginMessage;
import org.bitbucket.winteruo.server.messages.SupportedFeaturesMessage;
import org.bitbucket.winteruo.server.network.NetState;
import org.bitbucket.winteruo.server.network.NetStateManager;

public class GameServerLoginHandler implements IGameMessageListener {

	public void onMessageReceived(AbstractMessage message, INetState netState,
			ICore core) {


		GameServerLoginMessage gsLogin = (GameServerLoginMessage)message;

		NetState state =  NetStateManager.getInstance().getNetStateByAuthID(gsLogin.getAuthId());
		state.setSession(((NetState)netState).getSession());
		
		state.sendMessage(new SupportedFeaturesMessage());
		
		

		CharactersListMessage charaters = new CharactersListMessage(netState.getAccount());
		charaters.addCities(core.getMapsInfo().getCitiesInfo());
		charaters.addFlags(new Flag(20), new Flag(424));

		state.sendMessage(charaters);

		

	}



}
