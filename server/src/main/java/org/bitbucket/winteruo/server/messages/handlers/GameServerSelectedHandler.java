package org.bitbucket.winteruo.server.messages.handlers;

import java.util.Random;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.server.messages.ServerConnectMessage;

public class GameServerSelectedHandler implements IGameMessageListener {

	public void onMessageReceived(AbstractMessage message, INetState netState,
			ICore core) {
		
		//GameServerSelectedMessage gameServerMsg = (GameServerSelectedMessage)message;
		
		netState.setAuthID(new Random().nextInt() + 1);
		
		ServerConnectMessage connMsg = new ServerConnectMessage(core.getServerInfo(), netState.getAuthID());
		
		netState.sendMessage(connMsg);
	}
}
