package org.bitbucket.winteruo.server.messages.handlers;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.GetPlayerStatusType;
import org.bitbucket.winteruo.api.messages.enums.SendSkillsType;
import org.bitbucket.winteruo.server.components.World;
import org.bitbucket.winteruo.server.messages.GetPlayerStatusMessage;
import org.bitbucket.winteruo.server.messages.SendSkillsMessage;

public class GetPlayerStatusHandler implements IGameMessageListener {
	
	public void onMessageReceived(AbstractMessage message, INetState netState,
			ICore core) {
		
		GetPlayerStatusMessage gPl = (GetPlayerStatusMessage)message;
		
		
		if (gPl.getType() == GetPlayerStatusType.REQUEST_SKILLS)
			netState.sendMessage(new SendSkillsMessage(SendSkillsType.FULL_LIST, World.getInstance().getMobileBySerialId(netState.getCurrentCharacter().getMobileID()).getSkills()));
		
		
	}

}
