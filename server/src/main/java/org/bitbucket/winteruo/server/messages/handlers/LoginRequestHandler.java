package org.bitbucket.winteruo.server.messages.handlers;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.Account;
import org.bitbucket.winteruo.server.messages.GameServerEntryMessage;
import org.bitbucket.winteruo.server.messages.GameServerListMessage;
import org.bitbucket.winteruo.server.messages.LoginDeniedMessage;
import org.bitbucket.winteruo.server.messages.LoginRequestMessage;
import org.bitbucket.winteruo.server.messages.enums.LoginDeniedType;

public class LoginRequestHandler implements IGameMessageListener {

	public void onMessageReceived(AbstractMessage message, INetState netState, ICore core) {
		
		LoginRequestMessage loginReq = (LoginRequestMessage)message;
		
		
		Account account =  core.getAuthenticationManager().authenticate(loginReq.getUsername(), loginReq.getPassword());
		
		
		if (account == null)
			netState.sendMessage(new LoginDeniedMessage(LoginDeniedType.CredentialInvalid));
		else
		{
			netState.setAccount(account);
			GameServerListMessage serverList = new GameServerListMessage(new GameServerEntryMessage((short)0, core.getServerInfo()));
			
			netState.sendMessage(serverList);
		}
	}

	
}
