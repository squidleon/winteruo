package org.bitbucket.winteruo.server.messages.handlers;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.VersionInfo;
import org.bitbucket.winteruo.server.messages.LoginSeedMessage;

public class LoginSeedHandler implements IGameMessageListener {

	public void onMessageReceived(AbstractMessage message, INetState netState, ICore core) {
		
		LoginSeedMessage seedMsg = (LoginSeedMessage)message;
		
		netState.setVersionInfo(new VersionInfo(seedMsg.getClientMaj(), seedMsg.getClientMin(), seedMsg.getClientRev(), seedMsg.getClientPat()));
	
		
	}
	
	

}
