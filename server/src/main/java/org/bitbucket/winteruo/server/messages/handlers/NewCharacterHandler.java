package org.bitbucket.winteruo.server.messages.handlers;

import java.util.List;
import java.util.Set;

import org.bitbucket.winteruo.api.data.CityInfo;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.Direction;
import org.bitbucket.winteruo.api.model.SkillGame;
import org.bitbucket.winteruo.api.model.SkillInfo;
import org.bitbucket.winteruo.api.model.SkillLockFlag;
import org.bitbucket.winteruo.api.model.UOMobile;
import org.bitbucket.winteruo.server.components.World;
import org.bitbucket.winteruo.server.messages.ClientVersionMessage;
import org.bitbucket.winteruo.server.messages.NewCharacterMessage;

public class NewCharacterHandler implements IGameMessageListener {

	public void onMessageReceived(AbstractMessage message, INetState netState,
			ICore core) {

		NewCharacterMessage newCharacter = (NewCharacterMessage)message;
		int serialId = newCharacter.getNewCharacter().getMobileID();
		UOMobile mobile = World.getInstance().getMobileBySerialId(serialId);

		for (SkillGame skill: mobile.getSkills())
		{			
			skill.setBaseSkill(core.getFileManager().getSkillInfoById(skill.getBaseSkill().getSkillId()));
		}

		for (SkillInfo entry : core.getFileManager().getSkillsInfo())
		{
			if (!isSkillExsists(entry.getSkillId(), mobile.getSkills()) )
				mobile.getSkills().add(new SkillGame(entry, 1, 100, 100, SkillLockFlag.Up));

		}

		//newCharacter.getNewCharacter().setSkills(newSkills);
		mobile.setDirection(Direction.SOUTH);

		if (newCharacter.getNewCharacter().getStartupLocationId() == -1)
			newCharacter.getNewCharacter().setStartupLocationId(0);

		CityInfo city =  core.getMapsInfo().getCitiesInfo().get(newCharacter.getNewCharacter().getStartupLocationId());
		mobile.setX(city.getLocation().getX());
		mobile.setY(city.getLocation().getY());
		mobile.setZ(city.getLocation().getZ());
		mobile.setModelId((short)0x190);
		
		
		core.getAuthenticationManager().addCharacter(netState.getAccount(), newCharacter.getNewCharacter());

		netState.setAccount(core.getAuthenticationManager().reloadAccount(netState.getAccount().getSerialId()));


		netState.sendMessage(new ClientVersionMessage(netState.getVersionInfo()));

	}

	private boolean isSkillExsists(int id, Set<SkillGame> skills)
	{
		for (SkillGame skill : skills)
		{
			if (skill.getBaseSkill().getSkillId() == id)
				return true;
		}
		return false;
	}



}
