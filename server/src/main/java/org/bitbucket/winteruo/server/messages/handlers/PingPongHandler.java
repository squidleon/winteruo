package org.bitbucket.winteruo.server.messages.handlers;

import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.server.messages.PingPongMessage;

public class PingPongHandler implements IGameMessageListener {

	public void onMessageReceived(AbstractMessage message, INetState netState,
			ICore core) {
		
		PingPongMessage pingPing = (PingPongMessage)message;
	
		netState.sendMessage(pingPing);
		
	}
}
