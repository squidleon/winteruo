package org.bitbucket.winteruo.server.network;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.apache.mina.core.session.IoSession;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.model.Account;
import org.bitbucket.winteruo.api.model.AccountCharacter;
import org.bitbucket.winteruo.api.model.VersionInfo;

public class NetState implements INetState {

	@Getter @Setter
	private IoSession session;

	@Getter @Setter
	private int seed = -1;
	
	@Getter @Setter
	private boolean seeded;
	
	@Getter @Setter
	private int authID;
	
	@Getter @Setter
	private boolean firstPacketSend;
	
	@Getter @Setter
	private VersionInfo versionInfo;
	
	@Getter @Setter
	private Account account;
	
	@Getter @Setter
	private AccountCharacter currentCharacter;
	
	
	public boolean sendMessage(AbstractMessage message) {
		session.write(message);
		return true;
	}

	public boolean sendMessage(List<AbstractMessage> messages) {
		session.write(messages);
		return true;
	}

	public NetState(IoSession session) {
		this.session = session;
	}
	
	public void broadcastMessage(AbstractMessage message) {
		NetStateManager.getInstance().broadcastMessage(message);	
	}
	
	public void broadcastMessage(List<AbstractMessage> messages) {
	    NetStateManager.getInstance().broadcastMessage(messages);
	}

	



}
