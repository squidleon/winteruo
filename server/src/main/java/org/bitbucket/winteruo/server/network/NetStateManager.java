package org.bitbucket.winteruo.server.network;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

import org.apache.mina.core.session.IoSession;
import org.bitbucket.winteruo.api.messages.AbstractMessage;

import com.google.common.collect.Lists;


public class NetStateManager {

	private static NetStateManager _instance = null;
	public static NetStateManager getInstance() { if (_instance == null) _instance = new NetStateManager(); return _instance;}

	@Getter
	private List<NetState> sessions = new ArrayList<NetState>();

	public NetState getNetStateBySession(IoSession session)
	{
		for (NetState state: sessions )
		{
			if (state.getSession().equals(session))
				return state;
		}

		return null;
	}

	public void broadcastMessage(List<AbstractMessage> messages)
	{
		for (NetState state: sessions)
		{
			state.sendMessage(messages);
		}
	}
	
	public void broadcastMessage(AbstractMessage message)
	{
		broadcastMessage(Lists.asList(message,null));
	}
	
	public NetState getNetStateByAuthID(int authId)
	{
		for (NetState state: sessions )
		{
			if (state.getAuthID() == authId)
				return state;
		}

		return null;
	}
	
	public boolean disposeNetState(IoSession session)
	{
		NetState sessToRemove = getNetStateBySession(session);
		
		if (sessToRemove != null)
		{
			sessions.remove(sessToRemove);
			return true;
		}
		
		return false;
		
	}


	public NetState newNetState(IoSession session)
	{
		NetState netstate = getNetStateBySession(session);
		if (netstate == null)
		{
			netstate = new NetState(session);
			sessions.add(netstate);
		}

		return netstate;
	}


}
