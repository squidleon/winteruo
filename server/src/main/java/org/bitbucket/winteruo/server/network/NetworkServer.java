package org.bitbucket.winteruo.server.network;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import lombok.Getter;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.annotations.utils.ScanAnnotations;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.interfaces.INetState;
import org.bitbucket.winteruo.api.interfaces.INetworkServer;
import org.bitbucket.winteruo.api.listeners.IGameMessageListener;
import org.bitbucket.winteruo.api.listeners.INetworkServerListener;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.network.PackInfoDirection;
import org.bitbucket.winteruo.server.logger.WinterLogger;
import org.bitbucket.winteruo.server.messages.GameServerLoginMessage;
import org.bitbucket.winteruo.server.messages.GameServerSelectedMessage;
import org.bitbucket.winteruo.server.messages.GetPlayerStatusMessage;
import org.bitbucket.winteruo.server.messages.LoginCharacterMessage;
import org.bitbucket.winteruo.server.messages.LoginRequestMessage;
import org.bitbucket.winteruo.server.messages.LoginSeedMessage;
import org.bitbucket.winteruo.server.messages.NewCharacterMessage;
import org.bitbucket.winteruo.server.messages.PingPongMessage;
import org.bitbucket.winteruo.server.messages.handlers.CharacterLoginHandler;
import org.bitbucket.winteruo.server.messages.handlers.GameServerLoginHandler;
import org.bitbucket.winteruo.server.messages.handlers.GameServerSelectedHandler;
import org.bitbucket.winteruo.server.messages.handlers.GetPlayerStatusHandler;
import org.bitbucket.winteruo.server.messages.handlers.LoginRequestHandler;
import org.bitbucket.winteruo.server.messages.handlers.LoginSeedHandler;
import org.bitbucket.winteruo.server.messages.handlers.NewCharacterHandler;
import org.bitbucket.winteruo.server.messages.handlers.PingPongHandler;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

/**
 * Class for network server
 * @author tgiachi
 *
 */
public class NetworkServer implements INetworkServer {

	private static WinterLogger logger = WinterLogger.getLogger(NetworkServer.class);

	public static int BUFFER_SIZE = 2048;

	private ICore core;
	private IoAcceptor netServer;

	private ListMultimap<Byte, IGameMessageListener> gameMessageListeners = ArrayListMultimap.create();

	public HashMap<Byte, Class<AbstractMessage>> registeredMessages = new HashMap<Byte, Class<AbstractMessage>>();

	@Getter
	private List<INetworkServerListener> networkServerListeners = new ArrayList<INetworkServerListener>();

	public NetworkServer(ICore core) {
		this.core =core;
		scanMessages();
		registerDefaultHandlers();


	}

	private void registerDefaultHandlers()
	{
		register(LoginSeedMessage.CODE, new LoginSeedHandler());
		register(LoginRequestMessage.CODE, new LoginRequestHandler());
		register(GameServerSelectedMessage.CODE, new GameServerSelectedHandler());
		register(GameServerLoginMessage.CODE, new GameServerLoginHandler());
		register(NewCharacterMessage.CODE, new NewCharacterHandler());
		register(LoginCharacterMessage.CODE, new CharacterLoginHandler());
		register(GetPlayerStatusMessage.CODE, new GetPlayerStatusHandler());
		register(PingPongMessage.CODE, new PingPongHandler());
	}

	public void start() throws Exception
	{


		netServer = new NioSocketAcceptor();

		try {
			netServer.getFilterChain().addLast("protocol", new ProtocolCodecFilter(new WinterProtocolFactory(this, core)));

			logger.info("Network server is starting....");

			//if (core.getServerConfiguration().isDebug())
			//	netServer.getFilterChain().addLast( "logger", new LoggingFilter() );

			netServer.getSessionConfig().setUseReadOperation(true);

			netServer.getSessionConfig().setIdleTime( IdleStatus.BOTH_IDLE, 10 );
			netServer.getSessionConfig().setReadBufferSize(BUFFER_SIZE);

			netServer.setHandler(new WinterMessagesListener(core));

			netServer.bind(getConfigurationSocketAddress());

			logger.info("Listening on %s:%s",core.getServerConfiguration().getServerAddress(), core.getServerConfiguration().getServerPort());
			logger.info("Packets logging enabled = %s", core.getServerConfiguration().isPacketLogging());
			logger.info("Debug enabled = %s", core.getServerConfiguration().isDebug());

			logger.info("Network server started..");
		}
		catch(Exception ex)
		{
			throw new Exception(ex);
		}
	}

	public void stop() throws Exception {

		logger.info("Closing all sessions...");
		if (netServer.isActive()) {
			for (IoSession ss : netServer.getManagedSessions().values()) {
				ss.close(true);
			}

		}
		logger.info("Network server stopped");
	}

	public boolean addNetworkListener(INetworkServerListener listener)
	{
		return networkServerListeners.add(listener);
	}

	@SuppressWarnings("unchecked")
	private void scanMessages() 
	{
		try
		{
			Set<Class<?>> classes = ScanAnnotations.getAnnotationFor(PacketInfo.class, "org.bitbucket.winteruo.server.messages");

			for (Class<?> classz : classes)
			{
				if (classz.getSuperclass().equals(AbstractMessage.class))
				{
					PacketInfo annotation = classz.getAnnotation(PacketInfo.class);

					if (annotation.direction() == PackInfoDirection.BOTH || annotation.direction() == PackInfoDirection.FROM_CLIENT)
						registeredMessages.put(annotation.code(), (Class<AbstractMessage>) classz);
				}
			}

			logger.debug("Registered %s @PacketInfo messages", registeredMessages.size());

		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during scan for messages!: %s", ex.getMessage());
		}

	}

	public boolean register(byte code, IGameMessageListener listener) {

		return gameMessageListeners.put(code, listener);

	}


	public void printStatistics()
	{
		logger.info("Online users: %s", netServer.getManagedSessionCount());
	}

	public AbstractMessage getMessageByCode(byte code) {

		try
		{
			Class<AbstractMessage> classz = registeredMessages.get(code);

			if (classz != null)
			{
				AbstractMessage message = classz.newInstance();

				return message;

			}
		}
		catch(Exception ex)
		{
			logger.exception(ex, "Error during create new message code 0x%02X Error: %s", code, ex.getMessage());
		}

		return null;
	}


	public void notifyGameMessagesListeners(byte code, AbstractMessage message, INetState netstate)
	{
		if (gameMessageListeners.containsKey(code))
		{

			List<IGameMessageListener> listeners = gameMessageListeners.get(code);

			for (IGameMessageListener listener: listeners)
			{
				listener.onMessageReceived(message, netstate, core);
			}
		}
	}


	private InetSocketAddress getConfigurationSocketAddress() throws UnknownHostException
	{
		if (core.getServerConfiguration().getServerAddress().equals("0.0.0.0"))
			return new InetSocketAddress(core.getServerConfiguration().getServerPort());
		else
			return new InetSocketAddress(InetAddress.getByName(core.getServerConfiguration().getServerAddress()), core.getServerConfiguration().getServerPort());
	}



}
