package org.bitbucket.winteruo.server.network;

import java.nio.ByteBuffer;

import lombok.Getter;
import lombok.Setter;

import org.bitbucket.winteruo.api.annotations.PacketInfo;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.messages.enums.SeasonFlag;
import org.bitbucket.winteruo.api.network.PackInfoDirection;

@PacketInfo(code=SeasonChangeMessage.CODE, isCompress=true, direction=PackInfoDirection.FROM_SERVER)
public class SeasonChangeMessage extends AbstractMessage {

	private static final long serialVersionUID = 1L;

	public static final byte CODE = (byte) 0xBC;
	public static final int LENGHT = 3;
	
	@Getter @Setter
	private SeasonFlag season;
	private boolean playSound;
	
	
	public SeasonChangeMessage(SeasonFlag season, boolean playSound) {
		super(CODE);
		this.season = season;
		this.playSound = playSound;
	}
	
	@Override
	public int getLength() {
		return LENGHT;
	}
	
	
	@Override
	public byte[] getBytes() {
		ByteBuffer buffer = ByteBuffer.allocate(getLength());
		buffer.put(CODE);
		buffer.put((byte)season.ordinal());
		buffer.put((byte)(playSound == true ? 1 : 0));
		
		return buffer.array();
	
	}
	

}
