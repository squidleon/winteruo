package org.bitbucket.winteruo.server.network;

import java.util.List;

import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.server.logger.WinterLogger;

public class WinterMessagesListener implements IoHandler {

	private WinterLogger logger = WinterLogger.getLogger(getClass());

	private ICore core;


	public WinterMessagesListener(ICore core) {
		this.core = core;

	}


	public void sessionCreated(IoSession session) throws Exception {

	}

	public void sessionOpened(IoSession session) throws Exception {
		logger.info("Client %s connected", session.getRemoteAddress().toString());
		
		core.getNetworkServer().printStatistics();
	}

	public void sessionClosed(IoSession session) throws Exception {
		logger.info("Client %s disconnected", session.getRemoteAddress().toString());
		disposeNetState(session);
		
		core.getNetworkServer().printStatistics();

	}

	public void sessionIdle(IoSession session, IdleStatus status)
			throws Exception {

	}

	public void exceptionCaught(IoSession session, Throwable cause)
			throws Exception {
		
		logger.exception(new Exception(cause), "Error: %s", cause.getMessage());

	}

	@SuppressWarnings("unchecked")
	public void messageReceived(IoSession session, Object message)
			throws Exception {

		if (message instanceof List<?>)
		{

			List<AbstractMessage> incomingMessages = (List<AbstractMessage>) message;

			for (AbstractMessage incMessage: incomingMessages)
			{
				notifyListeners((AbstractMessage)incMessage, session);
			}
		}
		else
			if (message instanceof AbstractMessage)
			{
				notifyListeners((AbstractMessage)message, session);

			}
	}

	private void notifyListeners(AbstractMessage message, IoSession session)
	{
		NetState state = NetStateManager.getInstance().newNetState(session);

		core.getNetworkServer().notifyGameMessagesListeners(message.getCode(), message, state);
	}
	
	private void disposeNetState(IoSession session)
	{
		NetStateManager.getInstance().disposeNetState(session);
	}

	public void messageSent(IoSession session, Object message) throws Exception {

	}


}
