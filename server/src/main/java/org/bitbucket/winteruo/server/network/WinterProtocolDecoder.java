package org.bitbucket.winteruo.server.network;

import java.util.Arrays;

import lombok.AccessLevel;
import lombok.Getter;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.InvokeUtils;
import org.bitbucket.winteruo.api.utils.PacketReader;
import org.bitbucket.winteruo.server.logger.WinterLogger;


/**
 * Try to convert byte[] array in {@link AbstractMessage}
 * @author tgiachi
 *
 */
public class WinterProtocolDecoder extends CumulativeProtocolDecoder  {

	@Getter(value=AccessLevel.PROTECTED)
	private WinterLogger logger = WinterLogger.getLogger(getClass());

	private ICore core;
	private NetworkServer netServer;

	public WinterProtocolDecoder(NetworkServer server, ICore core) {
		this.core = core;
		this.netServer = server;
	}

	@Override
	protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {

		byte[] sbuffer = new byte[NetworkServer.BUFFER_SIZE];

		int nread = in.asInputStream().read(sbuffer, 0, NetworkServer.BUFFER_SIZE);
		if (nread != -1) {


			byte[] b = Arrays.copyOf(sbuffer, nread);

			notifyBytesReceived(session, b);

			//in.get(b);

			byte messageCode = b[0];

			AbstractMessage message = netServer.getMessageByCode(messageCode);

			//Class<AbstractMessage> abtClass = ServerCore.getMessages().get(messageCode);

			if (message != null)
			{
				//				AbstractMessage abMessage = abtClass.newInstance();
				//				abMessage.parse(b);
				//
				message.parse(new PacketReader(b, b.length));
				//logger.info("%s ToString = %s", message.getClass().getSimpleName(), message.toString());
				writeDebug(message, session);
				out.write(message);
			}
			else
			{
				getLogger().warn("Server <== Client: %s Unknown MessageID=0x%02X => Length: %s", session.getRemoteAddress().toString(),messageCode, b.length);
				//
			}
		}


		return true;
	}

	private void notifyBytesReceived(IoSession session, byte[] buffer)
	{
		if (core.getServerConfiguration().isPacketLogging())
		{
			InvokeUtils.invokeListeners(netServer.getNetworkServerListeners(), "onByteReceived", new Class<?>[] {String.class, buffer.getClass()},Long.toString(session.getId()), buffer);
		}

	}


	private void writeDebug(AbstractMessage message, IoSession session)
	{
		if (core.getServerConfiguration().isDebug())
			getLogger().info("Server <== Client: %s MessageClass: %s {%s} MessageID=0x%02X", session.getRemoteAddress().toString(),message.getClass().getSimpleName(), message.toString() ,message.getCode());
	}


}
