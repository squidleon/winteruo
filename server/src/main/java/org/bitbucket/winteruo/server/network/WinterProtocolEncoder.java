package org.bitbucket.winteruo.server.network;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;

import org.apache.commons.codec.binary.Hex;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.bitbucket.winteruo.api.interfaces.ICore;
import org.bitbucket.winteruo.api.messages.AbstractMessage;
import org.bitbucket.winteruo.api.utils.InvokeUtils;
import org.bitbucket.winteruo.server.logger.WinterLogger;
import org.bitbucket.winteruo.server.network.encoders.Huffman;



/**
 * Convert {@link AbstractMessage} to byte array
 * @author tgiachi
 *
 */
public class WinterProtocolEncoder implements ProtocolEncoder {

	@Getter(value=AccessLevel.PROTECTED)
	private WinterLogger logger = WinterLogger.getLogger(getClass());

	private ICore core;
	private NetworkServer netServer;

	public WinterProtocolEncoder(NetworkServer server, ICore core) {
		this.core = core;
		this.netServer = server;
	}

	public void encode(IoSession session, Object message,ProtocolEncoderOutput out) throws Exception {

		if (message instanceof AbstractMessage)
		{
			AbstractMessage abMessage = (AbstractMessage)message;
			sendMessge(session, out, abMessage);

		}
		else
			if (message instanceof List<?>)
			{
				List<AbstractMessage> messagesList = (List<AbstractMessage>)message;

				for (AbstractMessage msgToSend: messagesList)
				{
					sendMessge(session, out, msgToSend);
				}

			}

			else
			{
				getLogger().warn("Try to encode message class: %s!!", message.getClass().getSimpleName());
			}
	}

	private void sendMessge(IoSession session,ProtocolEncoderOutput out, AbstractMessage message) throws Exception
	{
		byte[] bufferToSend;

		if (message.isCompressed())
			bufferToSend = new Huffman().encode(message.getBytes());
		else
			bufferToSend = message.getBytes();


		IoBuffer buffer = IoBuffer.allocate(bufferToSend.length, true);
		buffer.put(bufferToSend);
		buffer.flip();

		out.write(buffer);
		
	    
	     
		notifyBytesSendedListeners(session, bufferToSend);

		writeDebug(session, message,  bufferToSend);
		notifyMessagesSendedListeners(session, message);

	}

	private void writeDebug(IoSession session, AbstractMessage message, byte[] buffer) 
	{
		if (core.getServerConfiguration().isDebug())

		{  
			byte[] codebytes = new byte[1];
			codebytes[0] = buffer[0];
			String code = Hex.encodeHexString(codebytes);
			getLogger().info("Server ==> Client: %s MessageClass: %s MessageID=0x%s => Length: %s", session.getRemoteAddress().toString(), message.getClass().getSimpleName(), code.toUpperCase(), buffer.length);
		}
	}

	public void notifyMessagesSendedListeners(IoSession session, AbstractMessage message)
	{
		if (core.getServerConfiguration().isPacketLogging())
		{
			InvokeUtils.invokeListeners(netServer.getNetworkServerListeners(), "onMessageSended", new Class<?>[] {String.class, AbstractMessage.class},Long.toString(session.getId()), message);
		}
	}

	public void notifyBytesSendedListeners(IoSession session, byte[] buffer)
	{
		if (core.getServerConfiguration().isPacketLogging())
		{
			InvokeUtils.invokeListeners(netServer.getNetworkServerListeners(), "onByteSended", new Class<?>[] {String.class, buffer.getClass()},Long.toString(session.getId()), buffer);
		}
	}

	public void dispose(IoSession session) throws Exception {
		// nothing to dispose
	}

}
