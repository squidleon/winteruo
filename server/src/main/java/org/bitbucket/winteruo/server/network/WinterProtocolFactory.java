package org.bitbucket.winteruo.server.network;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.bitbucket.winteruo.api.interfaces.ICore;


/**
 * Glue all together {@link WinterProtocolDecoder} and @{link {@link WinterProtocolEncoder}
 * @author tgiachi
 *
 */
public class WinterProtocolFactory implements ProtocolCodecFactory  {


	private WinterProtocolEncoder wpEncoder;
	private WinterProtocolDecoder wpDecoder;


	public ProtocolEncoder getEncoder(IoSession session) throws Exception {
		return wpEncoder;
	}

	public ProtocolDecoder getDecoder(IoSession session) throws Exception {
		return wpDecoder;
	}

	public WinterProtocolFactory(NetworkServer server,  ICore core) {
		wpEncoder = new WinterProtocolEncoder(server, core);
		wpDecoder = new WinterProtocolDecoder(server, core);
	}



}
