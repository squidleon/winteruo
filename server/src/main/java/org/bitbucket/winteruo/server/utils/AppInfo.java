package org.bitbucket.winteruo.server.utils;


/**
 * Class where stored the Application informations
 * To get application's version use <b>getVersion()</b>
 * 
 * @author tgiachi
 * 
 * 
 * 
 *
 */
public class AppInfo {

	public final static String APP_NAME = "WinterUO";
	public final static String APP_VERSION = "0.0.1-SNAPSHOT";
	public final static String APP_BUILD = "20130829";
	public final static String APP_WEBSITE = "https://bitbucket.org/squidleon/winteruo";
	/**
	 * Get readable version String
	 * @return
	 */
	public static String getApplication() {
		return String.format("%s %s (%s)", AppInfo.APP_NAME, AppInfo.APP_VERSION, AppInfo.APP_BUILD);
	}
	
}
